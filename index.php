<?php
if(isset($_SERVER['HTTP_ORIGIN'])){
    header('Access-Control-Allow-Origin:'.$_SERVER['HTTP_ORIGIN']);
}
//header('Access-Control-Allow-Origin: *'); // *代表允许任何网址请求
// 响应类型
header('Access-Control-Allow-Methods:*');
header('Access-Control-Allow-Headers:x-requested-with,content-type,authorization'); 

define('ROOT', realpath('./'));
defined('DS') or define('DS', DIRECTORY_SEPARATOR);

require_once(ROOT.DS.'yaoxs'.DS.'autoload.php');
