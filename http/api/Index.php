<?php
namespace http\api;

use yaoxs\Controller;
use http\service\Article;
use http\service\ArticleCategory;

// 请求与响应的相关方法
class Index extends Controller{
    use \http\traitobj\Jwt;

    private $articleService = null;
    private $articleCategoryService = null;

    public function __construct(){
        $this->articleService = new Article;
        $this->articleCategoryService = new ArticleCategory;
    }

    // 获取P(平台)C(控制器)A(方法名)
    public function index(){
        $article = $this->articleService->getPageData(' id,title,chapter,article_category_id,file_html ');
        $count = $this->articleService->count();
        $articleCategory = $this->articleCategoryService->getAll();
        $this->echoJson([
            'article' => $article,
            'articleCategory' => $articleCategory,
            'count' => $count
        ]);
    }

    public function getAllArticleCategory(){
        $articleCategory = $this->articleCategoryService->getAll();
        $this->echoJson([
            'articleCategory' => $articleCategory
        ]);
    }

    public function getArticleList(){
        $article = $this->articleService->getPageData(' id,title,chapter,article_category_id,file_html ');
        $count = $this->articleService->count();
        $this->echoJson([
            'article' => $article,
            'count' => $count
        ]);
    }

    public function getArticleRow(){
        $row = $this->articleService->row();
        $this->echoJson($row);
    }
}
