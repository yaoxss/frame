<?php
namespace http\api;

use yaoxs\Controller;
use http\service\Article;
use http\service\ArticleCategory;
use http\service\MdAnalysis;
use http\service\Login as LoginServic;

// 请求与响应的相关方法
class AdminArticle extends Controller{
    use \http\traitobj\Jwt;

    private $articleService = null;
    private $articleCategoryService = null;
    private $loginService = null;

    public function __construct(){
        $this->articleService = new Article;
        $this->articleCategoryService = new ArticleCategory;
        $this->loginService = new LoginServic;
        // 验证权限暂时写在控制器里
        $token = $_SERVER['HTTP_AUTHORIZATION'];
        if(!$this->loginService->verifyToken($token)){
            echo "Unauthorized";die;
        }
    }

    public function index(){
        $info = $this->articleService->getPageData('id,title,article_category_id');
        $count = $this->articleService->count();
        $this->echoJson([
            'data' => $info,
            'count' => $count
        ]);
    }

    public function create(){
        $info = $this->articleService->create();
        $this->echoStatusJson($info);
    }

    public function getAllArticleCategory(){
        $info = $this->articleCategoryService->getAll('id,title');
        $this->echoJson($info);
    }

    public function getArticleRow(){
        $row = $this->articleService->row();
        $this->echoJson($row);
    }

    public function update(){
        $info = $this->articleService->update();
        $this->echoStatusJson($info);
    }

    public function createTemplate(){
        $row = $this->articleService->row();
        $template = file_get_contents(ROOT.DS.'web'.DS.'template.html');
        $headTitle = $row['title'] ?? '';
        $title = $row['title'] ?? '';
        $m = new MdAnalysis;
        $content = $row['content'] ?? "### 數據錯誤，請聯係管理員 ###end";
        // $content = explode("\n",$content);
        // var_dump($content);die;
        $content = $m->eenderingMd($content);
        // echo $content;die;
        
        // var_dump($content);die;
        // 這裏應該可以優化，只是暫時寫死
        $template = str_replace('<{$headTitle}>',$headTitle,$template);
        $template = str_replace('<{$title}>',$title,$template);
        $template = str_replace('<{$content}>',$content,$template);
        // 創建文件
        if(!file_exists(ROOT.DS.'web'.DS.'details')){
            mkdir(ROOT.DS.'web'.DS.'details',0777,true);
        }
        if(isset($row['id'])){
            $fileName = md5($row['id'].'yaoxs').'.html';
            file_put_contents(ROOT.DS.'web'.DS.'details'.DS.$fileName, $template);
            $this->articleService->updateField($row['id'],['file_html' => $fileName,'updated_at' => date('Y-m-d H:i:s')]);
            $this->echoStatusJson(true);
        }
        $this->echoStatusJson(false);
    }
}
