<?php
namespace http\api;

use yaoxs\Controller;
use http\service\Article;
use http\service\ArticleCategory;
use http\service\Login as LoginServic;

// 请求与响应的相关方法
class AdminArticleCategory extends Controller{
    use \http\traitobj\Jwt;

    private $articleService = null;
    private $articleCategoryService = null;
    private $loginService = null;

    public function __construct(){
        $this->articleService = new Article;
        $this->articleCategoryService = new ArticleCategory;
        $this->loginService = new LoginServic;
        // 验证权限暂时写在控制器里
        $token = $_SERVER['HTTP_AUTHORIZATION'];
        if(!$this->loginService->verifyToken($token)){
            echo "Unauthorized";die;
        }
    }

    public function index(){
        $info = $this->articleCategoryService->getPageData('id,title');
        $count = $this->articleCategoryService->count();
        $this->echoJson([
            'data' => $info,
            'count' => $count
        ]);
    }

    public function create(){
        $info = $this->articleCategoryService->create();
        $this->echoStatusJson($info);
    }

    public function row(){
        $row = $this->articleCategoryService->row();
        $this->echoJson($row);
    }

    public function update(){
        $info = $this->articleCategoryService->update();
        $this->echoStatusJson($info);
    }
}
