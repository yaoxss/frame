<?php
namespace http\api;

use yaoxs\Controller;
use http\service\Login as LoginServic;

// 请求与响应的相关方法
class Login extends Controller{

    private $loginService = null;

    public function __construct(){
        $this->loginService = new LoginServic;
    }

    public function login(){
        $token = $this->loginService->login();
        if($token == false){
            echo 'Unauthorized';
            die;
        }
        $this->echoJson([
            'access_token' => $token
        ]);
    }

}
