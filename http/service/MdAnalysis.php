<?php
namespace http\service;

// md富文本解析 成标准的html
class MdAnalysis{
    private $snippetType = ['#','##','###','####','#####','######','```p','```code'];
    private $symbolCut = ['<','>','/',':','px','rem',';','(',')','=','{','}','.'];
    private $arr9CDCFE = ['public','function','protected','private','new','let'];
    private $arrC586C0 = ['if','else','elseif','return','for','foreach'];
    private $arr678080 = ['<','>','/','-','{','}','.'];
    private $arr569CD6 = ['html','title','meta','h1','h2','h3','h4','h5','h6','span','p','div','head','style','script','ul','li','table','tr','td','br','body','px','rem'];
    private $arr7BB8DA = ["and","align-content","align-items","align-self","all","animation","animation-delay","animation-direction","animation-duration","animation-fill-mode","animation-iteration-count","animation-name","animation-play-state","animation-timing-function","backface-visibility","background","background-attachment","background-blend-mode","background-clip","background-color","background-image","background-origin","background-position","background-repeat","background-size","border","border-bottom","border-bottom-color","border-bottom-left-radius","border-bottom-right-radius","border-bottom-style","border-bottom-width","border-collapse","border-color","border-image","border-image-outset","border-image-repeat","border-image-slice","border-image-source","border-image-width","border-left","border-left-color","border-left-style","border-left-width","border-radius","border-right","border-right-color","border-right-style","border-right-width","border-spacing","border-style","border-top","border-top-color","border-top-left-radius","border-top-right-radius","border-top-style","border-top-width","border-width","bottom","box-decoration-break","box-shadow","box-sizing","break-after","break-before","break-inside","caption-side","caret-color","@charset","clear","clip","clip-path","color","column-count","column-fill","column-gap","column-rule","column-rule-color","column-rule-style","column-rule-width","column-span","column-width","columns","content","counter-increment","counter-reset","cursor","direction","display","empty-cells","filter","flex","flex-basis","flex-direction","flex-flow","flex-grow","flex-shrink","flex-wrap","float","font","@font-face","font-family","font-feature-settings","@font-feature-values","font-kerning","font-language-override","font-size","font-size-adjust","font-stretch","font-style","font-synthesis","font-variant","font-variant-alternates","font-variant-caps","font-variant-east-asian","font-variant-ligatures","font-variant-numeric","font-variant-position","font-weight","grid","grid-area","grid-auto-columns","grid-auto-flow","grid-auto-rows","grid-column","grid-column-end","grid-column-gap","grid-column-start","grid-gap","grid-row","grid-row-end","grid-row-gap","grid-row-start","grid-template","grid-template-areas","grid-template-columns","grid-template-rows","hanging-punctuation","height","hyphens","image-rendering","@import","isolation","justify-content","@keyframes","left","letter-spacing","line-break","line-height","list-style","list-style-image","list-style-position","list-style-type","margin","margin-bottom","margin-left","margin-right","margin-top","mask","mask-type","max-height","max-width","@media","min-height","min-width","mix-blend-mode","object-fit","object-position","opacity","order","orphans","outline","outline-color","outline-offset","outline-style","outline-width","overflow","overflow-wrap","overflow-x","overflow-y","padding","padding-bottom","padding-left","padding-right","padding-top","page-break-after","page-break-before","page-break-inside","perspective","perspective-origin","pointer-events","position","quotes","resize","right","scroll-behavior","tab-size","table-layout","text-align","text-align-last","text-combine-upright","text-decoration","text-decoration-color","text-decoration-line","text-decoration-style","text-indent","text-justify","text-orientation","text-overflow","text-shadow","text-transform","text-underline-position","top","transform","transform-origin","transform-style","transition","transition-delay","transition-duration","transition-property","transition-timing-function","unicode-bidi","user-select","vertical-align","visibility","white-space","widows","width","word-break","word-spacing","word-wrap","writing-mode","z-index"];

    public function eenderingMd($str){
        $content = $str;
        // let snippetType = snippetType;
        $contentSplitArray = [];
        $arr = [];
        // echo $content;die;
        while($content != ''){
            $arr = $this->splitContent($content,$this->snippetType);
            $contentSplitArray[] = ['key' => $arr[0],'value' => $arr[1]];
            $content = $arr[2];
            // var_dump($content);die;
        }
        // var_dump($contentSplitArray);die;
        return $this->renderingMdbox($contentSplitArray);
    }

    /**
     * return [a,b,c]
     * a content首次出现的片段类型
     * b content首次出现的片段内容
     * c content删除b后的剩下内容
     * */ 
    public function splitContent($content,$snippetType){
        $sSnippetLocation = null;
        $eSnippetLocation = null;
        $sSnippetLocationType = ''; // #
        $eSnippetLocationType = ''; // #end
        foreach($snippetType as $item){
            $location = strpos($content,$item);
            if( $location !== false && ($sSnippetLocation === null || $location <= $sSnippetLocation)){
                $sSnippetLocation = $location + strlen($item);
                $sSnippetLocationType = $item;
            }
        }
        $eSnippetLocationType =  $sSnippetLocationType.'end';
        $eSnippetLocation = strpos($content,$eSnippetLocationType);
        if($eSnippetLocation <= $sSnippetLocation+1){
            // 这一步判断是避免出现死循环的情况
            return [
                '```p',
                '字符串异常：异常位置：' . $eSnippetLocation,
                ''
            ];
        }
        return [
            0 => $sSnippetLocationType,
            1 => substr($content,$sSnippetLocation,$eSnippetLocation-$sSnippetLocation),
            2 => substr($content,$eSnippetLocation + strlen($eSnippetLocationType))
        ];
    }

    public function renderingMdbox($contentSplitArray){
        $innerText = "";
        foreach($contentSplitArray as $item){
            $className = $this->getSnippetClassName($item['key']);
            $dom = '';
            if($item['key'] == '```code'){
                $dom .= $this->codeMachining($item['value']);
            }else{
                $dom = "<p class='{$className}' >";
                $dom .= htmlspecialchars($item['value']);
                $dom .= '</p>';
            }
            $innerText = $innerText . $dom;
        }
        // echo $innerText;die;
        return $innerText;
    }

    private function codeMachining($str){
        $pre = '<pre>';
        $rowList = explode("\n",$str);
        unset($rowList[count($rowList)-1]);
        unset($rowList[0]);
        foreach($rowList as $key => $item){
            $wordList = explode(' ',$item);
            // var_dump($wordList);die;
            foreach($wordList as $index => $word){
                if($word == ' ' || $word == ''){
                    $pre .= ' ';
                }else{
                    if($index != 0 && $word == ' ' && $word == ''){
                        $pre .= ' ';
                    }
                    $word = explode("\t",$word);
                    // var_dump($word);die;
                    foreach($word as $val){
                        if($val == ''){
                            $pre .= '    ';
                        }else{
                            // $domArr = $this->labelProcessing($val);
                            $htmlspecialchars = htmlspecialchars($val);
                            $pre .= "<span>{$htmlspecialchars}</span>";
                        }   
                    }
                }
            }
            $pre .= "<br>";
        }
        // var_dump($pre.'</pre>');die;
        // var_dump($pre);die;
        return $pre.'</pre>';
    }

    public function labelProcessing($str){
        $prefix = 'yaoxs-md-code-span';
        $arrDom = [];
        $indexOfArr = [];
        foreach($this->symbolCut as $i){
            if(strpos($str,$i) !== false){
                $indexOfArr[strpos($str,$i)] = $i;
            }
        }
        $previousPoint = 0;
        foreach($indexOfArr as $key => $value){
            if($key == 0){
                // 说明是第一个
                $htmlspecialchars = htmlspecialchars($value);
                $className = $prefix . $this->getWordColorClassName($value);
                $span = "<span class='{$className}' >" . $htmlspecialchars . "</span>";
                $arrDom[] = $span;
                $previousPoint += strlen($htmlspecialchars);
            }else{
                $htmlspecialchars = htmlspecialchars($value);
                $className = $prefix . $this->getWordColorClassName(substr($htmlspecialchars,$previousPoint,$key - $previousPoint));
                $span = "<span class='{$className}' >" . substr($htmlspecialchars,$previousPoint,$key - $previousPoint) . "</span>";
                $arrDom[] = $span;
                // 加上他本身 < or > or = ....
                $className = $prefix . $this->getWordColorClassName($value);
                $span2 = "<span class='{$className}' >" . $htmlspecialchars . "</span>";
                $arrDom[] = $span2;
                $previousPoint = $key + strlen($value);
            }
        }
        if($previousPoint < strlen($str)){
            // 说明在 < or > or = 后面还有值的情况
            $className = $prefix . $this->getWordColorClassName(substr($str,$previousPoint,strlen($str)));
            $span = "<span class='{$className}' >" . substr($str,$previousPoint,strlen($str)) . "</span>";
            $arrDom[] = $span;
        }
        return $arrDom;
    }

    private function getWordColorClassName($str){
        $arr9CDCFE = $this->arr9CDCFE;
        $arrC586C0 = $this->arrC586C0;
        $arr678080 = $this->arr678080;
        $arr569CD6 = $this->arr569CD6;
        $arr7BB8DA = $this->arr7BB8DA;
        if(in_array($str,$arr9CDCFE) !== false) return '-9CDCFE';
        if(in_array($str,$arrC586C0) !== false) return '-C586C0';
        if(in_array($str,$arr678080) !== false) return '-678080';
        if(in_array($str,$arr569CD6) !== false) return '-569CD6';
        if(in_array($str,$arr7BB8DA) !== false) return '-7BB8DA';
        return '';
    }

    private function getSnippetClassName($key){
        $prefix = "yaoxs-md-";
        /* ['#','##','###','####','#####','######','```p','```code'] */
        if($key == "#") return $prefix.'h1';
        if($key == "##") return $prefix.'h2';
        if($key == "###") return $prefix.'h3';
        if($key == "####") return $prefix.'h4';
        if($key == "#####") return $prefix.'h5';
        if($key == "######") return $prefix.'h6';
        if($key == "```p") return $prefix.'p';
        if($key == "```code") return $prefix.'code';
        return $prefix.'异常';
    }
}