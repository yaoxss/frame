<?php
namespace http\service;
use yaoxs\DB;
use yaoxs\Http;
// use http\trait\common;

// 请求与响应的相关方法
class Article{
    use \http\traitobj\Common;
    
    protected $db;
    private $http = null;
    private $post = [];
    
    public function __construct(){
        $this->http = new Http;
        $this->db = new DB();
        $post = $this->http->P();
        if(!empty($post['form'])){
            $post['form'] = json_decode(htmlspecialchars_decode(trim($post['form'])),true);
        }
        $this->post = $post;
    }
    
    public function getPageData($field = '*'){
        $page = intval($this->post['page'] ?? 1);
        $size = intval($this->post['size'] ?? 20);
        $s = ($page - 1) * $size;
        $where = $this->whereBind($this->post['form'] ?? []);
        $sql = "select {$field} from article where {$where['where']} limit {$s},{$size}";
        $article = $this->db->select($sql,$where['bind']);
        // 这里可以使用in查询出需要的数据（分类数据不多所以没有使用）
        $sql = "select id,title from article_category";
        $category = $this->db->select($sql);
        $category = $this->arrayKeyValConversion($category,'id','title');
        foreach($article as &$value){
            $value['article_category_title'] = $category[$value['article_category_id']] ?? '-';
        }
        return $article;
    }

    public function whereBind($array){
        $where = ' 1=1 ';
        $bind = [];
        $article_category_id = ' and 1 = 1 ';
        $title = " and 1=1 ";
        if(!empty($array['article_category_id'])){
            $article_category_id = " and article_category_id = :article_category_id";
            $bind[':article_category_id'] = $array['article_category_id'];
        }
        $where .= $article_category_id;
        if(!empty($array['title'])){
            $title = " and title like :title ";
            $bind[':title'] = "%{$array['title']}%";
        }
        $where .= $title;
        return [
            'where' => $where,
            'bind' => $bind
        ];
    }

    public function count(){
        $where = $this->whereBind($this->post['form'] ?? []);
        $sql = "select count(id) as count from article where {$where['where']} ";
        return $this->db->select($sql,$where['bind'])[0]['count'];
    }

    public function row(){
        $post = $this->http->P();
        if(!isset($post['id'])){
            return false;
        }
        $sql = "select * from article where id = :id ";
        $bind = [
            ':id' => $post['id']
        ];
        return $this->db->select($sql,$bind)[0];
    }

    public function create(){
        $post = $this->http->P();
        // var_dump($post);die;
        // 图片上传未做
        $inster = [
            'title' => $post['title'] ?? '',
            'chapter' => $post['chapter'] ?? '',
            'author' => $post['author'] ?? '',
            'content' => $this->http->getpost()['content'] ?? '',
            'article_category_id' => $post['article_category_id'] ?? 1,
            'created_at' => date('Y-m-d H:i:s')
        ];
        return $this->db->insert('article',$inster);
    }

    public function update(){
        $post = $this->http->P();
        $update = [
            'title' => $post['title'],
            'chapter' => $post['chapter'],
            'author' => $post['author'],
            'content' => $this->http->getpost()['content'],
            'article_category_id' => $post['article_category_id'],
            'updated_at' => date('Y-m-d H:i:s')
        ];
        $whereBind = [
            ':id' => $post['id']
        ];
        return $this->db->update('article',$update, 'id = :id',$whereBind);
    }

    public function updateField($id,$update){
        $whereBind = [
            ':id' => $id
        ];
        return $this->db->update('article',$update, 'id = :id',$whereBind);
    }


}
