<?php
namespace http\service;
use yaoxs\DB;
use yaoxs\Http;
// use http\trait\common;

// 请求与响应的相关方法
class ArticleCategory{
    use \http\traitobj\Common;
    
    protected $db;
    private $http = null;
    
    public function __construct(){
        $this->http = new Http;
        $this->db = new DB();
    }

    public function getAll($field = "*"){
        $sql = "select {$field} from article_category";
        return $this->db->select($sql);
    }
    
    public function getPageData($field = '*'){
        $post = $this->http->P();
        $page = $post['page'] ?? 1;
        $size = $post['size'] ?? 20;
        // var_dump($post);die;
        $s = ($page - 1) * $size;
        $sql = "select {$field} from article_category limit {$s},{$size}";
        $data = $this->db->select($sql);
        return $data;
    }

    public function whereBind($array){
        $where = ' 1=1 ';
        $bind = [];
        $title = " and 1=1 ";
        if(!empty($array['title'])){
            $title = " and title like :title ";
            $bind[':title'] = "%{$array['title']}%";
        }
        $where .= $title;
        return [
            'where' => $where,
            'bind' => $bind
        ];
    }

    public function count(){
        $where = $this->whereBind($this->post['form'] ?? []);
        $sql = "select count(id) as count from article_category where {$where['where']} ";
        return $this->db->select($sql,$where['bind'])[0]['count'];
    }

    public function row(){
        $post = $this->http->P();
        if(!isset($post['id'])){
            return false;
        }
        $sql = "select * from article_category where id = :id ";
        $bind = [
            ':id' => $post['id']
        ];
        return $this->db->select($sql,$bind)[0];
    }

    public function create(){
        $post = $this->http->P();
        $inster = [
            'title' => $post['title'],
            'created_at' => date('Y-m-d H:i:s')
        ];
        return $this->db->insert('article_category',$inster);
    }

    public function update(){
        $post = $this->http->P();
        $update = [
            'title' => $post['title'],
            'updated_at' => date('Y-m-d H:i:s')
        ];
        $whereBind = [
            ':id' => $post['id']
        ];
        return $this->db->update('article_category',$update, 'id = :id',$whereBind);
    }
}
