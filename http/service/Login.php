<?php
namespace http\service;
use yaoxs\DB;
use yaoxs\Http;
// use http\trait\common;

// 请求与响应的相关方法
class Login{
    use \http\traitobj\Jwt;
    
    protected $db;
    protected $privateKey;
    protected $type;
    protected $effectiveTime;
    protected $email;
    private $http = null;
    
    public function __construct(){
        $this->privateKey = md5('test');
        $this->email = '*****';
        $this->effectiveTime = 3600 * 24;
        $this->http = new Http;
        $this->db = new DB();
    }
    
    public function login(){
        $post = $this->http->P();
        $email = $post['email'] ?: false;
        $password = $post['password'] ?: false;
        if(!$email || !$password){
            return false;
        }
        $sql = " select * from users where email = :email ";
        $user = $this->db->select($sql,[':email' => $email]);
        if(!password_verify($password, $user[0]['password'])){
            return false;
        }
        if(count($user) != 1 || $user[0]['email'] != $this->email){
            return false;
        }
        $user = $user[0];
        $token_array = array(
            'email'=> $user['email'],
            'password'=> $user['password'],
            'time'=>time()
        );
        $token = $this->jwt(json_encode($token_array),'ENCODE',$this->privateKey);
        return $token;
    }

    public function verifyToken($token){
        $token = $this->jwt($token, 'DECODE', $this->privateKey);
        $token = json_decode($token, true);
        $verifyTokenF = isset($token['time']) && isset($token['email']) && isset($token['password']);
        if(!$verifyTokenF && $token['time'] + $this->effectiveTime <= time() ){
            // token 未能正常解析或者 超過有效時間
            return false;
        }
        if($token['email'] !== $this->email && count($token) != 3){
            return false;
        }
        return true;
    }

}
