<?php
namespace http\traitobj;

trait Common{
    
    public function arrayKeyValConversion($array,$keyConversion,$valueConversion){
        $returnArray = [];
        foreach($array as $value){
            $returnArray[$value[$keyConversion]] = $value[$valueConversion];
        }
        return $returnArray;
    }
}