-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2022-03-31 14:48:01
-- 服务器版本： 5.6.50-log
-- PHP 版本： 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `blog`
--

-- --------------------------------------------------------

--
-- 表的结构 `article`
--

CREATE TABLE `article` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '标题',
  `img_path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '文章图片路径',
  `chapter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '文章的描述',
  `author` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '作者',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `article_category_id` tinyint(3) DEFAULT '1' COMMENT '文章类别ID',
  `content` text COLLATE utf8mb4_unicode_ci COMMENT '文章内容'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 转存表中的数据 `article`
--

INSERT INTO `article` (`id`, `title`, `img_path`, `chapter`, `author`, `created_at`, `updated_at`, `deleted_at`, `article_category_id`, `content`) VALUES
(1, 'CSS3 @media 查询 和 rem像素', NULL, 'rem 单位是一个相对大小的长度，它是根据html标签的字体大小来决定长度（浏览器的默认字体大小为16px）。也就是说当html里面的字体越大，rem的长度就越长。', 'yaoxs', '2021-07-13 07:55:20', '2021-11-22 06:11:37', NULL, 1, '### CSS3 @media 查询 和 rem像素 ###end\n```p\n参考链接：https://www.runoob.com/cssref/css3-pr-mediaquery.html\n```pend\n\n```code\n/** 如果文档宽度小于 300 像素则修改背景颜色(background-color):*/\n@media screen and (max-width: 300px) {\n    body {\n        background-color:lightblue;\n    }\n}\n```codeend\n```p\n参考链接：https://blog.csdn.net/lvyang251314/article/details/82798858\nrem 单位是一个相对大小的长度，它是根据html标签的字体大小来决定长度（浏览器的默认字体大小为16px）。也就是说当html里面的字体越大，rem的长度就越长。\n```pend\n\n```code\n<!DOCTYPE html>\n<html lang=\"zh\">;\n<head>\n	<meta charset=\"UTF-8\">\n	<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n	<meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n	<title></title>\n</head>\n<style type=\"text/css\">\n	html{\n		font-size: 16px;\n	}\n	.box {\n		font-size: 1rem;\n		width: 10rem;\n		height: 100px;\n		background-color: blue;\n	}\n</style>\n<body>\n	<div class=\"box\">\n		ccccc\n	</div>\n</body>\n</html>\n```codeend\n\n```p\n如果app页面需要手机和平板都兼容的时候，如果只使用px，可能他在手机端的视觉效果大小刚好，但是平板电脑里面的视觉效果就显得太小了。说白了就是要根据显示屏幕的大小来调整html的字体大小来决定适配rem大小比例\n```pend\n\n```code\nhtml{font-size:10px}\n@media screen and (min-width:321px) and (max-width:375px){html{font-size:11px}}\n@media screen and (min-width:376px) and (max-width:414px){html{font-size:12px}}\n@media screen and (min-width:415px) and (max-width:639px){html{font-size:15px}}\n@media screen and (min-width:640px) and (max-width:719px){html{font-size:20px}}\n@media screen and (min-width:720px) and (max-width:749px){html{font-size:22.5px}}\n@media screen and (min-width:750px) and (max-width:799px){html{font-size:23.5px}}\n@media screen and (min-width:800px){html{font-size:25px}}\n```codeend'),
(10, '命名空间', NULL, '1、用户编写的代码与PHP内部的类/函数/常量或第三方类/函数/常量之间的名字冲突。  \r\n2、为很长的标识符名称(通常是为了缓解第一类问题而定义的)创建一个别名（或简短）的名称，提高源代码的可读性。\r\n注意：命名空间并没有引入文件的功能', 'yaoxs', '2021-07-18 19:16:19', '2021-11-23 03:49:02', NULL, 2, '### 命名空间的作用和解决的问题 ###end\n```p\n1、用户编写的代码与PHP内部的类/函数/常量或第三方类/函数/常量之间的名字冲突。  \n2、为很长的标识符名称(通常是为了缓解第一类问题而定义的)创建一个别名（或简短）的名称，提高源代码的可读性。\n注意：命名空间并没有引入文件的功能\n```pend\n\n#### a\\index.php ####end\n```code\n<?php\nnamespace a;\n\nclass Index{\n    public function getdesc(){\n        return \'我是a文件夹下的index.php文件\';\n    }\n}\n?>\n```codeend\n\n#### b\\index.php ####end\n```code\n<?php\nnamespace b;\n\nclass Index{\n    public function getdesc(){\n        return \'我是b文件夹下的index.php文件\';\n    }\n}\n?>\n```codeend\n\n#### index.php ####end\n\n```code\n<?php\n// include __DIR__ . \'\\a\\index.php\';\n// include __DIR__ . \'\\b\\index.php\';\nspl_autoload_register(\'autoload\',true,true);\nfunction autoload($className){\n    include __DIR__.DIRECTORY_SEPARATOR.strtolower($className).\'.php\';\n}\n\nuse a\\Index;\n$obj = new Index();\n// 我是a文件夹下的index.php文件\necho $obj->getdesc();\n\necho \"<br/>\";\n\nuse b\\Index as Bindex;\n$obj = new Bindex();\n// 我是b文件夹下的index.php文件\necho $obj->getdesc();\n```codeend'),
(11, 'js常用函数封装', NULL, '', 'yaoxs', '2021-08-02 23:08:12', '2022-03-01 04:14:25', NULL, 1, '### 根据时间戳返回日期 ###end\n```code\n<script>\nfunction add0(m){return m<10?\'0\'+m:m }\nfunction format(shijianchuo)\n{\n    //shijianchuo是整数，否则要parseInt转换\n    var time = new Date(shijianchuo);\n    var y = time.getFullYear();\n    var m = time.getMonth()+1;\n    var d = time.getDate();\n    var h = time.getHours();\n    var mm = time.getMinutes();\n    var s = time.getSeconds();\n    return y+\'-\'+add0(m)+\'-\'+add0(d)+\' \'+add0(h)+\':\'+add0(mm)+\':\'+add0(s);\n}\n</script>\n```codeend\n\n### 当月时间戳 ###end\n```code\n<script>\nlet time = new Date();\nlet year = time.getFullYear();\nlet month = parseInt( time.getMonth() + 1 );\nthis.form.date = new Date( year + \"-\" + month + \"-01 00:00:00\" ).getTime();\n</script>\n```codeend\n\n### 字符串驗證浮點數并且返回保留兩位小數的浮點數 ###end\n```code\n<script>\nfunction returnMoneyMunber(str) {\n  let value = parseFloat(str);\n  if(isNaN(value) || value <= 0){\n    return \'0.00\';\n  }else{\n    value += \'\';\n    let arr = value.split(\'.\');\n    if(arr.length == 1){\n      return arr[0] + \'.00\';\n    }else{\n      if(arr[1].length == 1){\n        return arr[0] + \'.\' +arr[1] + \'0\';\n      }else{\n        return arr[0] + \'.\' + arr[1][0] + arr[1][1];\n      }\n    }\n  }\n}\nreturnMoneyMunber(\'a\'); // \'0.00\'\nreturnMoneyMunber(\'3\'); // \'3.00\'\nreturnMoneyMunber(\'3.2\'); // \'3.20\'\nreturnMoneyMunber(\'3.2a\'); // \'3.20\'\n</script>\n```codeend'),
(42, 'Promise（期约）', NULL, '', 'yaoxs', '2021-12-06 07:17:32', '2021-12-10 09:52:14', NULL, 1, '### Promise（期约） ###end\n```p\nlet p = new Promise(() => {});\nconsole.log(p);  //  返回一个对象 Promise <pending>\n\n期约的三种状态\npending(待定) \nfulfilled(兑现) 或 resolved(解决)\nrejected(拒绝)\n```pend\n```p\n通过执行函数控制期约的状态\n```pend\n```code\nlet p1 = new Promise((resolve, reject) => {\n	resolve();\n	// resolve()和 reject()中的哪个被调用，状态转换都不可撤销了。\n	reject(); // 没有效果\n});\nconsole.log(p1);  //  此时p1的状态为fulfilled\n```codeend\n```p\n通过setTimeout避免期约卡在pending(待定) 状态下\n```pend\n```code\nlet p = new Promise((resolve, reject) => { \n	setTimeout(reject, 10000); // 10 秒后调用 reject() \n	// 执行函数的逻辑\n	// 有时候请求时间过长或者失败 无法执行resolve();\n	// 那么这个期约10秒后就会从待定切换到拒绝状态\n});\n```codeend\n```p\n调用Promise.resolve()静态方法实例化一个解决期约\n```pend\n```code\nlet p2 = new Promise((resolve, reject) => resolve(Promise.resolve(\'a\')));\np2.then((b) => {console.log(b)} ); // a\n// 对这个静态方法而言，如果传入的参数本身是一个期约，那它的行为就类似于一个空包装。\nlet p = Promise.resolve(7); \nsetTimeout(console.log, 0, p === Promise.resolve(p)); \n// true \nsetTimeout(console.log, 0, p === Promise.resolve(Promise.resolve(p))); \n// true\n```codeend\n```p\n在ECMAScript 暴露的异步结构中，任何对象都有一个 then()方法。这个方法被认为实现了Thenable 接口。\n```pend\n```code\nclass MyThenable { \n then() {} \n}\n```codeend\n```p\nPromise.prototype.then()\n返回一个新的期约实例：\n参数：onResolved 处理程序和 onRejected 处理程序。\n如果提供的话，则会在期约分别进入“兑现”和“拒绝”状态时执行\n在一个解决期约上调用 then()会把 onResolved 处理程序推进消息队列。但这个处理程序在当前线程上的同步代码执行完成前不会执行。因此，跟在 then()后面的同步代码一定先于处理程序执行。\n```pend\n```code\nlet p1 = new Promise((resolve, reject) => resolve(\'resolve\')); \nlet p2 = new Promise((resolve, reject) => reject(\'reject\'));\np1.then((a) => console.log(a) ,(a) => console.log(a) );\np2.then((a) => console.log(a) ,(a) => console.log(a) );\np2.then(null,(a) => console.log(a));\n// 若调用 then()时不传处理程序，则原样向后传，传处理程序则Promise.resolve()会包装默认的返回\n值 undefined。\nlet p1 = Promise.resolve(\'foo\'); \n// 若调用 then()时不传处理程序，则原样向后传\nlet p2 = p1.then();   // Promise <resolved>: foo\nlet p3 = p1.then(() => undefined); //  Promise <resolved>: undefined\n\n// 抛出异常会返回拒绝的期约：\nlet p10 = p1.then(() => { throw \'baz\'; }); \n// Uncaught (in promise) baz \nsetTimeout(console.log, 0, p10); // Promise <rejected> baz\n\n\nPromise.prototype.catch()\nPromise.prototype.catch()方法用于给期约添加拒绝处理程序。这个方法只接收一个参数：\nonRejected 处理程序。事实上，这个方法就是一个语法糖，调用它就相当于调用 Promise.prototype. \nthen(null, onRejected)。\n\nlet p = Promise.reject(); \nlet onRejected = function(e) { \n setTimeout(console.log, 0, \'rejected\'); \n}; \n// 这两种添加拒绝处理程序的方式是一样的：\np.then(null, onRejected); // rejected \np.catch(onRejected); // rejected\n```codeend\n```p\nPromise.prototype.finally()\n这个处理程序在期约转换为解决或拒绝状态时都会执行。这个方法可以避免 onResolved 和 onRejected 处理程序中出现冗余代码。但 onFinally 处理程序没有办法知道期约的状态是解决还是拒绝，所以这个方法主要用于添加清理代码。\n```pend\n```code\nlet p1 = Promise.resolve(); \nlet onFinally = function() { \n setTimeout(console.log, 0, \'Finally!\') \n} \np1.finally(onFinally); // Finally\n```codeend\n```p\n非重入期约方法(期约生命周期)\n即使期约状态变化发生在添加处理程序之后，处理程序也会等到运行的消息队列让它出列时才会执行。\n```pend\n```code\nlet synchronousResolve; \n// 创建一个期约并将解决函数保存在一个局部变量中\nlet p = new Promise((resolve) => { \nconsole.log(\'0:resolve\');  \nsynchronousResolve = function() { \n console.log(\'1: invoking resolve()\'); \n resolve(); \n console.log(\'2: resolve() returns\'); \n }; \n}); \np.then(() => console.log(\'4: then() handler executes\')); \nsynchronousResolve(); \nconsole.log(\'3: synchronousResolve() returns\');\n\n// 如果给期约添加了多个处理程序，当期约状态变化时，相关处理程序会按照添加它们的顺序依次执行。无论是 then()、catch()还是 finally()添加的处理程序都是如此。\n```codeend\n```p\n期约连锁(异步任务串行化)\n把期约逐个地串联起来是一种非常有用的编程模式。之所以可以这样做，是因为每个期约实例的方法（then()、catch()和 finally()）都会返回一个新的期约对象，而这个新期约又有自己的实例方法。这样连缀方法调用就可以构成所谓的“期约连锁”。\n```pend\n```code\nfunction delayedResolve(num) { \n return new Promise((resolve, reject) => { \n console.log(num+\' executor\'); \n resolve(num);\n }); \n}\ndelayedResolve(1) \n .then(() => delayedResolve(2)) \n .then(() => delayedResolve(3)) \n .then(() => delayedResolve(4))\n// 每个后续的处理程序都会等待前一个期约解决，然后实例化一个新期约并返回它。这种结构可以简洁地将异步任务串行化，解决之前依赖回调的难题。\n// 疑问：如果中途有一个期约失败会怎么样\n```codeend\n```code\n<script>\nexport default {\n    name: \'Tinymce\',\n    props: {},\n    data () {\n        return {\n            images_upload_handler: (blobInfo, succFun, failFun) => {\n                // 转化为易于理解的file对象\n                // blob\n                var file = blobInfo.blob();\n                let data = {};\n                if(file.size < 1024 * 1024){\n                    data.file = file;\n                    this.post(\'/api/upload/index\',{file: file}).then(res => {\n                        succFun(this.resolveAssetsUrl(res.location[0]));\n                    }).catch(e => {\n                        // 圖片上傳失敗\n                        failFun(\'圖片上傳失敗\');\n                    })\n                }else{\n                    this.readImg(file).then((a) => {\n                        this.compress(a,file).then((blob) => {\n                            this.post(\'/api/upload/index\',{file: blob}).then(res => {\n                                succFun(this.resolveAssetsUrl(res.location[0]));\n                            }).catch(e => {\n                                // 圖片上傳失敗\n                                failFun(\'圖片上傳失敗\');\n                            })\n                        });\n                    });    \n                }\n            },\n        }\n    },\n    methods: {\n        // 图片压缩\n        compress (image,file,maxwidth = 2000, maxheigh = 2000) {\n            return new Promise((resolve, reject) => {\n                const canvas = document.createElement(\'canvas\')\n                const ctx = canvas.getContext(\'2d\')\n\n                let { width, height } = image\n                const scale = width / height\n                if (scale > 1) {\n                    if (width > maxwidth) {\n                        width = maxwidth\n                        height = maxwidth / scale\n                    }\n                } else {\n                    if (height > maxheigh) {\n                        height = maxheigh\n                        width = maxheigh * scale\n                    }\n                }\n                canvas.width = width\n                canvas.height = height\n                ctx.fillRect(0, 0, width, height)\n                \n                 //将img绘制到画布上\n                ctx.drawImage(image, 0, 0, width, height)\n                const compressData = canvas.toDataURL(image.type || \'image/jpeg\', 0.9)\n                resolve(this.dataURLtoFile(compressData, file.name))\n            })\n        },\n        // 将file转换成img对象\n        readImg(file) {\n            return new Promise((resolve, reject) => {\n                const img = new Image()\n                const reader = new FileReader()\n                reader.onload = function(e) {\n                   img.src = e.target.result\n                }\n                reader.onerror = function(e) {\n                   reject(e)\n                }\n                reader.readAsDataURL(file)\n                img.onload = function() {\n                   resolve(img)\n                }\n                img.onerror = function(e) {\n                   reject(e)\n                }\n            })\n        },\n        // dataURL输出成文件\n        dataURLtoFile (dataURL, fileName) {\n            const arr = dataURL.split(\',\')\n            const mime = arr[0].match(/:(.*?);/)[1]\n            const bstr = atob(arr[1])\n            let n = bstr.length\n            const u8arr = new Uint8Array(n)\n            while (n--) {\n                u8arr[n] = bstr.charCodeAt(n)\n            }\n            return new File([u8arr], fileName, { type: mime })\n        },\n        resolveAssetsUrl\n    }\n}\n</script>\n```codeend\n```p\nPromise.all()\nPromise.all()静态方法创建的期约会在一组期约全部解决之后再解决。这个静态方法接收一个可迭代对象，返回一个新期约\n待续\n```pend'),
(31, 'HTTPS的故事', NULL, '一些教程经常提到 POST 比 GET提交数据更安全，真正从安全的角度来说，只要是 Http 请求，都不安全 。（补充 @王泥煤 的回复 post的确比get更安全，这一点与https无关。因为用get方式传密码会被日志系统记录下明文，或者被第三方的referer带出去，即便你采用了https）', 'yaoxs', '2021-08-09 00:05:23', '2021-11-23 03:52:25', NULL, 1, '```p\n原文： https://blog.csdn.net/hou_manager/article/details/78774767?spm=1001.2014.3001.5501\n```pend\n\n```p\nhttps折中的方法是先使用非对称加密，最后使用对称加密（原因是对称加密速度快，非对称加密比较慢）。\n对称加密和非对称加密的速度？\n\n小谷(浏览器)->小王（恶意伪造者）->小美(服务器)\n小谷(浏览器 公钥加密)->小王（恶意伪造者）->小美(服务器 私钥解密)(如果是服务器发送到浏览器不可行)\n小谷(浏览器 公钥)->生成对称加密AES密钥->公钥加密AES密钥->小王（恶意伪造者）->小美(服务器私钥解密AES密钥) （浏览器和服务器彼此之间形成对称密钥加密数据）\n小谷(浏览器 公钥（小王给的）)->生成对称加密AES密钥->公钥加密AES密钥->小王（恶意伪造者 (用自己的私钥解密后，用服务器的公钥加密向服务器发送) ）->小美(服务器私钥解密AES密钥) \n\n# 这一步感觉 如果 小谷 ->小王（恶意伪造者）->CA 一样存在漏洞\n小美(服务器 向CA申请证书（名字(Hash)、学号(Hash)、公钥(Hash)、公证人班长的名字((Hash))） )->班长(CA 通过自己的私钥 对数据进行处理)\n小谷(浏览器) -> 公证人班长(CA) -> 小谷(浏览器 用CA的公钥对数据进行解密，再对元素数据进行Hash和解密的数据进行对比，来确认公钥属于指定服务器)\n\n故事完了。故事中 小谷是浏览器，小美是服务器，小花和小王是坏蛋，班长是 CA 机构，班主任是上级 CA，校长是根 CA 服务器。当一个网站要使用 https 时先需在一些国际认证的 CA 机构填写网站信息申请证书，而这些 CA机构往往还有上层 CA，最终有一个根 CA。一般来说浏览器都会内置根 CA 和一些顶级 CA 的证书，但需要验证的时候会通过 CA 链逐级验证。\n\n公钥和私钥（数据进行公钥加密私钥解密，或者私钥加密公钥解密）\n```pend\n\n```p\n一些教程经常提到 POST 比 GET提交数据更安全，真正从安全的角度来说，只要是 Http 请求，都不安全 。（补充 @王泥煤 的回复 post的确比get更安全，这一点与https无关。因为用get方式传密码会被日志系统记录下明文，或者被第三方的referer带出去，即便你采用了https）\n\n如果你连一个公共场所的 wifi，连接该 wifi 的所有人都可以嗅探你发的网络请求的明文数据，包括用户名和密码\n即使你是在家上网，中间任何环节都能查看、修改你网络请求的明文数据，比如你的路由器、小区的网关、ISP…\n只要使用 Http协议，不论做任何安全措施都是徒劳的，只有Https 协议才保证数据在传输过程中的安全。\n\n问题来了，什么是安全？\n\n数据传输的安全其实体现在以下几个方面：\n\n我发的数据，不该看的人看不懂\n\n我发的数据，不该看的人不能伪造或者修改(修改后接收方可察觉)\n\n我发的数据，过期之后不该看的人不能偷偷拿过来继续用\n\nHttps 怎样保证数据传输安全？再讲讲 Https的原理之前必须先聊一聊数据加密，目前有以下几种加密方式：\n\n对称加密 ： 加密和解密数据使用同一个密钥。这种加密方式的优点是速度很快，常见对称加密的算法有 AES；\n非对称加密： 加密和解密使用不同的密钥，叫公钥和私钥。数据用公钥加密后必须用私钥解密，数据用私钥加密后必须用公钥解密。一般来说私钥自己保留好，把公钥公开给别人，让别人拿自己的公钥加密数据后发给自己，这样只有自己才能解密。 这种加密方式的特点是速度慢，CPU 开销大，常见非对称加密算法有 RSA；\nHash： hash 是把任意长度数据经过处理变成一个长度固定唯一的字符串，但任何人拿到这个字符串无法反向解密成原始数据（解开你就是密码学专家了），Hash 常用来验证数据的完整性。常见 Hash 算法有 MD5（已经不安全了）、SHA1、SHA256\n\n理解了加密方式后我们就能利用这些算法实现安全数据传输。以一个故事开始：\n\n班里来了个新同学小美，长的特别漂亮，小谷暗恋小美很久，终于有一天小谷鼓起勇气向小美表白。小谷写了个纸条：“我是小谷，我喜欢你，你喜欢我吗？” ，让小王转交给小美。\n\n正常情况下，小王会把纸条转给小美，小美看到后很开心，回复了纸条：“我是小美，我也喜欢你”，让小王再转递给小谷。\n\n可是小王也暗恋小美，当然不愿甘做嫁衣。这个时候小王可以有几方法来捣乱：\n\n收到小谷的纸条后扔掉纸条。但这样做小谷收不到回应下课会去单独问小美，事情就败露了\n偷窥纸条的内容，然后举报给老师\n收到小谷的纸条后，小王立即给小谷发一个回复纸条：“我不喜欢你，我喜欢小王”。同时小王给小美发一个纸条：“我是小谷，你长的真丑，嘿嘿”\n正常帮小谷和小美投递纸条，成就美事。但复印一份小谷的纸条，过两天小王把这个纸条发给了班里的其他女生小花，这样小美知道后必然会闹分手\n\n小谷很聪明，早就看出来小王心怀不轨，可是没办法，要传信必然要经过小王，要怪就怪自己太腼腆不敢下课直接亲手送上。怎么办呢？\n\n有一种办法是把纸条的信息加密(AES 加密)，小王就不知道纸条内容了。可是问题来了，小美也不知道密钥，她收到信后也无法解密啊。倒是可以把密钥放到纸条里，可这样小王也得到了密钥等于白忙活一场。\n\n再说说小美，小美很漂亮，在以前的学校被称为数学女神，转学到这里后立即被小谷吸引了，她看出来小谷有点喜欢自己但又不十分确定，她不想主动。她猜到小谷会在上课的时候可能用写纸条的方式表白，也猜到猥琐的小王会在中间捣乱。如何让小谷能顺利向自己表白呢？小美在疯狂的思考：\n\n方案一： 我可以生成一对公钥和私钥，把公钥公开给班里所有的同学。这样小谷就能得到公钥，把要发的信息用公钥加密，只有我自己能用私钥解密。可是我如何把我的回复发出去呢？如果用我的私钥加密，小谷倒是能用公钥解密，但班里任何同学都能解密，被其他同学看到就糗大了。而且如果小谷纸条的内容太多，他用公钥加密需要花一整节课的时间，不能耽误他学习\n\n方案二：我可以生成一对公钥和私钥，把公钥公开给班里所有的同学，这样小谷就能得到公钥。如果小谷够聪明的话，他得到公钥后会生成一个对称加密AES的密钥，然后用我的公钥把 AES 密钥加密(AES 密钥长度很短加密几乎不需要时间)，然后把加密后的密钥发给我。我收到后用我的私钥解密，得到小谷和我约定的对对称加密密钥，然后我们用这个只有我们俩知道的对称密钥加密数据进行交流。\n\n下课休息的时候，小美给班里的同学说：“快数学考试了，我的数学很好，考场上需要要我帮忙的同学可以找我，我公布一下我的公钥，为了安全起见大家用我的公钥加密消息。”\n\n一切貌似天衣无缝，可是小美在班里公布自己公钥的时候小谷正好拉肚子去了厕所，回来之后才知道这事，小谷于是问旁边的同学小花小美公钥是多少。小花以前是班里受欢迎的女生被很多男生仰慕，在小美来之后立即被夺了风头，一直怀恨在心。于是小花告诉了小谷一个假公钥，其实是自己生成的公钥。这样万一小谷找小美要小抄，自己可以冒充小美给一个错答案。\n\n一切被小美看到眼里，这样不行啊， 别到时候自己和小谷的好事没成，反而同学考试被陷害栽赃到自己头上。怎么办呢？对了，班长是班里人品很正的人，可以用班长给自己做信用背书，正好前两天班长在班里通知了自己的公钥。 于是小美找到班长，做了一个证书，证书上有自己的名字、学号、公钥、公证人班长的名字，同时把这些信息通过 Hash (sha256)处理后请求班长用自己的私钥进行加密（为什么用 Hash 处理？因为东西太多非对称加密很费时），也放到证书上。于是小美再次把证书通报给班里所有的同学。\n\n小谷这次得到了证书，发现上面的公证人是班长，于是先用班长的公钥对证书上被班长私钥加密后的字段进行解密，得到元素数据的 Hash。 再对元素数据进行 Hash和刚刚解密的 Hash 进行比对。如果比对成功表示证书上的信息无误，且是由班长担保的。这时候小谷确信证书上的公钥就是小美的。任何人因为没有班长的私钥都无法伪造证书。\n\n你以为故事就这么圆满结束了吗？其实没有，班长前两天在公布自己的证书的时候小谷正好生病请假了。 他根本不知道班长的公钥是不是他本人的。\n\n这个时候他发现班长的证书上有班主任的担保（班主任的公钥做了签名），班主任的证书上有校长的担保，而小谷是有校长的的公钥的，就印在学生证上。\n\n终于，小谷收获了女神的芳心。\n\n故事完了。故事中 小谷是浏览器，小美是服务器，小花和小王是坏蛋，班长是 CA 机构，班主任是上级 CA，校长是根 CA 服务器。当一个网站要使用 https 时先需在一些国际认证的 CA 机构填写网站信息申请证书，而这些 CA机构往往还有上层 CA，最终有一个根 CA。一般来说浏览器都会内置根 CA 和一些顶级 CA 的证书，但需要验证的时候会通过 CA 链逐级验证。\n```pend'),
(32, 'redis查询今天过去一年内来某个城市的人数', NULL, '假设只有每一天都提供某城市的人员名单。这时时候，建立一个有序集合保存每天提供的城市的人员名单，并且集合保存的是身份证ID不会出现重复。这时候存在的问题是假设10年后的今天，某城市只有2千万人口，而实际上集合里面却有1亿甚至更多。', 'yaoxs', '2021-08-14 04:23:01', '2021-11-23 03:56:10', NULL, 7, '### 查询今天过去一年内来某个城市的人数 ###end\n\n```p\n# 假设只有每一天都提供某城市的人员名单。\n# 这时时候，建立一个有序集合保存每天提供的城市的人员名单，并且集合保存的是身份证ID不会出现重复。\n# 这时候存在的问题是假设10年后的今天，某城市只有2千万人口，而实际上集合里面却有1亿甚至更多。\n# 会出现这样的原因是，虽然集合保存的是每一天的城市人口，但是并没有清除出去的人口。\n# 我的理解是：\n# 给集合的每一个元素设置一个时间戳的分数，值保存的是身份证id\n# 这样的话，每一天的成员进来时都会，重新赋值一个分数也就是时间戳\n# zrangebyscore(返回有序集合中指定分数区间的成员列表)。\n# zremrangebyscore (用于移除有序集中，指定分数（score）区间内的所有成员)\n# zcard（用于计算集合中元素的数量）\n# 当我们需要获取今天过去一年内来城市的人数时，就可以使用zrangebyscore(返回有序集合中指定分数区间的成员列表)。\n# 或者使用zremrangebyscore删除一年外的数据，然后使用zcard获取集合中元素的数量\n```pend\n\n```p\n----------\n```pend\n\n\n### 第二种方法 ###end\n```p\n# 通过字符型的形式创建并且利用有效时间创建\n# 用一个空的数据库，保存这些人员名单。\n# 通过set的方式将城市的人员名单逐一放到redis里面并且设置有效时间为1年。\n# 这样的话，有效时间一年后的人员将被自动清除。\n# 最后通过dbsize获取这个数据仓库key的数量。\n# 另外一种就是使用keys进行模糊查询返回符合条件的所有key的值。\n```pend'),
(33, 'PHP时间的相关操作', NULL, '关于获取当天时间0点或者其他', 'yaoxs', '2021-10-11 02:27:26', '2021-11-23 03:57:14', NULL, 2, '### 获取指定某一天的0点和23点59分59秒 ###end\n```code\n$search[\'addtime\'] = \'2021-10-12\'\n$s = strtotime(date(\'Y-m-d\',strtotime($search[\'addtime\'])));\n$e = $s + 24 * 3600 - 1;\n$addtime = \"and ( addtime >= ? and addtime <= ? ) \";\n```codeend\n\n### PHP获取某个月第一天/最后一天代码 ###end\n```code\nfunction getthemonth($date){\n   $firstday = date(\'Y-m-01\', strtotime($date));\n   $lastday = date(\'Y-m-d\', strtotime(\"$firstday +1 month -1 day\"));\n   return array($firstday, $lastday);\n}\n```codeend'),
(47, 'CSS图片处理的相关属性', NULL, '', 'yaoxs', '2021-12-20 06:32:59', '2021-12-20 07:21:44', NULL, 1, '### object-fit ###end \n```p\nhttps://developer.mozilla.org/zh-CN/docs/Web/CSS/object-fit\nobject-fit CSS 属性指定可替换元素的内容应该如何适应到其使用的高度和宽度确定的框。\nobject-fit: fill | contain | cover | none | scale-down\nfill: 被替换的内容正好填充元素的内容框。整个对象将完全填充此框。如果对象的宽高比与内容框不相匹配，那么该对象将被拉伸以适应内容框。\ncontain: 被替换的内容将被缩放并保持宽高比例，如果原图比例和替换比例不一致，上下或者左右将有一个留空，留空的颜色由背景颜色属性决定。\ncover: 被替换的内容在保持其宽高比的同时填充元素的整个内容框。如果原图比例和替换比例不一致，纵向或者横向将会有一边被裁剪。\nnone: 被替换的内容将保持其原有的尺寸。\nscale-down: 内容的尺寸与 none 或 contain 中的一个相同，取决于它们两个之间谁得到的对象尺寸会更小一些。\n```pend\n### object-position ###end\n```p\nobject-position 规定了可替换元素的内容，在内容框中的位置。可替换元素的内容框中未被对象所覆盖的部分，则会显示该元素的背景（background）。\n默认 object-position: 50% 50%;\nobject-position: right top;\nobject-position: left bottom;\nobject-position: 250px 125px;\n/* Global values */\nobject-position: inherit;\nobject-position: initial;\nobject-position: unset;\n```pend'),
(48, 'JS數組相關函數', NULL, '', 'yaoxs', '2022-01-10 07:39:45', '2022-01-10 07:55:03', NULL, 1, '```p \nsplice删除或替换现有元素或者原地添加新的元素来修改数组,并会会改变原数组。\n返回值：以数组形式返回被修改的内容。\n```pend\n```code\nconst months = [\'Jan\', \'March\', \'April\', \'June\'];\nmonths.splice(1, 0, \'Feb\');\n// inserts at index 1\nconsole.log(months);\n// expected output: Array [\"Jan\", \"Feb\", \"March\", \"April\", \"June\"]\n\nmonths.splice(4, 1, \'May\');\n// replaces 1 element at index 4\nconsole.log(months);\n// expected output: Array [\"Jan\", \"Feb\", \"March\", \"April\", \"May\"]\n\n// 删除数组的第一个元素\nmonths.splice(0, 1)\n// expected output: Array [\'Feb\', \'March\', \'April\', \'May\']\n```codeend'),
(49, 'CSS 盒子模型', NULL, '', 'yao', '2022-01-21 01:36:02', '2022-01-21 02:52:55', NULL, 1, '## CSS 盒子模型 ##end \n```p\n参考链接：https://www.cnblogs.com/clearsky/p/5696286.html\ncss盒子模型 包含 外边距 + 边框 + 内边距 + 元素内容\n其中背景区域为：边框 + 内边距 + 元素内容，外边距margin是透明的并不会遮挡其他元素。\n元素框的总宽度 = 元素（element）的width + padding的左边距和右边距的值 + margin的左边距和右边距的值 + border的左右宽度；\n元素框的总高度 = 元素（element）的height + padding的上下边距的值 + margin的上下边距的值 ＋ border的上下宽度。\n```pend\n```code\n<style>\n    .div{\n        width: 300px;\n        height: 300px;\n        margin: 10px;\n        padding: 10px;\n        border: 1px solid #000;\n        background-color: aquamarine;\n    }\n</style>\n<body>\n    <div class=\"div\"></div>\n</body>\n```codeend\n### CSS外边距叠加 ###end \n```p\n在普通文档流中垂直相遇时外边距会叠加，也就是两个元素之间的外边距的距离等于以元素大的外边距。而浮动、定位等并不会叠加外边距。\n例如：普通文档流的两个div，一个div的外边距是10px,另一个外边距是20px，那么这两个div之间的距离是20px。可以理解为10px与20px重合。\n这种外边距的存在意义在于段落与段落之间的外边距，如果没有叠加，那么段落之间便是2倍的距离，如果叠加那么段落与段落之间便是一倍的距离。\n```pend\n### 父子元素 第一个和最后一个子元素margin上下越界问题 ###end\n```p\n当父元素没有设置border or padding时第一个和最后一个子元素上下margin便会出现margin溢出问题，也就是如果这时候如果给父元素设置border、padding、或者给父元素添加 overflow：hidden都会存在副作用而且又可能不适合。解决的办法是父元素加前置内容生成。\n```pend\n```code\n<style>\n    * {\n        margin: 0;\n        padding: 0;\n    }\n    .div{\n        width: 300px;\n        height: 300px;\n        /* margin: 10px; */\n        /* padding: 10px; */\n        /* border: 1px solid #000; */\n        background-color: aquamarine;\n    }\n    .div:before{\n        content : \"\";\n        display: table;\n    }\n    .child-div{\n        width: 200px;\n        height: 200px;\n        margin: 20px;\n        background-color: burlywood;\n    }\n</style>\n<body>\n    <div class=\"div\">\n        <div class=\"child-div\"></div>\n    </div>\n</body>\n```codeend\n### 浏览器间的盒子模型 ###end\n```p\n(1)ul标签在Mozilla中默认是有padding值的，而在IE中只有margin有值。\n(2)标准盒子模型与IE模型之间的差异：\n　　标准的盒子模型就是上述介绍的那种，而IE模型更像是 box-sizing : border-box; 其内容宽度还包含了border和padding。解决办法就是：在html模板中加doctype声明。\n```pend\n### box-sizing属性 ###end\n```p\nbox-sizing 属性最大的好处便是对盒子模型的计算进行简化\nbox-sizing : content-box|border-box|inherit;\ncontent-box(默认) 总宽度(或高度)=margin+border+padding+width(or height)\nborder-box 总宽度(或高度)=margin+width(or height) 换句话说使元素内容的宽度或者高度包含内边距和边框的大小\ninherit 规定应从父元素继承 box-sizing 属性的值\n```pend'),
(36, 'css 变量用法和js的调用方式', NULL, 'css变量的基本用法', 'yaoxs', '2021-10-14 19:15:09', '2021-11-23 04:00:41', NULL, 1, '```p\n参考网址：https://www.ruanyifeng.com/blog/2017/05/css-variables.html\n```pend\n\n### 定义css变量 ###end\n```code\n<style>\n# 全局的变量通常放在根元素:root里面\n:root{\n  --leftMenuShow: \'block\';\n  --leftMenuButtonShow: \'none\';\n  --leftMenuButtonCloseShow: \'none\';\n}\nbody{\n  --leftMenuShow: \'block\';\n  --leftMenuButtonShow: \'none\';\n  --leftMenuButtonCloseShow: \'none\';\n}\n.top{\n  --leftMenuShow: \'block\';\n  --leftMenuButtonShow: \'none\';\n  --leftMenuButtonCloseShow: \'none\';\n}\n</style>\n```codeend\n\n### 调用css变量 ###end\n```code\n<style>\ndiv{\n    display: var(--leftMenuButtonShow);\n    padding:8px;\n}\n</style>\n```codeend\n\n### JavaScript操作 ###end\n```code\n// 检测浏览器是否支持 CSS 变量\nconst isSupported = window.CSS && window.CSS.supports && window.CSS.supports(\'--a\', 0);\n\nif (isSupported) {\n  /* supported */\n} else {\n  /* not supported */\n}\n\n// 设置变量\ndocument.body.style.setProperty(\'--primary\', \'#7F583F\');\ndocument.documentElement.style.setProperty(\'--leftMenuShow\', \'block\');\n\n// 读取变量\ndocument.body.style.getPropertyValue(\'--primary\').trim();\ngetComputedStyle(document.documentElement).getPropertyValue(\'--leftMenuShow\'); \n\n// 删除变量\ndocument.body.style.removeProperty(\'--primary\');\n```codeend'),
(37, 'PHP导出excel内存溢出的问题', NULL, '', 'yaoxs', '2021-10-20 01:44:22', '2021-11-23 04:02:49', NULL, 2, '### 使用的excel扩展：https://xlswriter-docs.viest.me/zh-cn/reader/sheet_list ###end\n\n### 优化前代码 ###end\n```code\npublic function downloadOrderExcela(){\n        ini_set(\'memory_limit\',\'128M\');\n\n        $get = $this->G();\n        $db = M();\n        // dump($post);die;\n        // file_put_contents(\"test.txt\", \'----\'.json_encode($get).\"\\r\\n\",FILE_APPEND);\n        $search = $this->searchIndex($get);\n        // dump($search);die;\n        $field = implode(\',\',array_keys($this->returnFieldAnnotation()));\n        $data = $db->table(\'orders\')->field($field)->where($search[\'where\'])->bind($search[\'bind\'])->limit(\'0,15000\')->findAll();\n        $obj =  new form_options;\n        $order_type = $this->conversionArray($obj->getOrderType());\n        $return_status = $this->conversionArray($obj->getReturnStatus());\n        foreach($data as &$val){\n            $val[\'receipt_time\'] = $val[\'receipt_time\'] > 0 ? date(\'Y-m-d H:i\',$val[\'receipt_time\']) : \'-\';\n            $val[\'out_single_time\'] = $val[\'out_single_time\'] > 0 ? date(\'Y-m-d H:i\',$val[\'out_single_time\']) : \'-\';\n            $val[\'delivery_time\'] = $val[\'delivery_time\'] > 0 ? date(\'Y-m-d H:i\',$val[\'delivery_time\']) : \'-\';\n            $val[\'order_type\'] = $order_type[$val[\'order_type\']] ?? \'-\';\n            $val[\'return_status\'] = $return_status[$val[\'return_status\']] ?? \'-\';\n        }\n\n        $excelData = [];\n        $arr = array_values($this->returnFieldAnnotation());\n        array_push($excelData,$arr);\n        foreach($data as $value){\n            $arr = array_values($value);\n            array_push($excelData,$arr);\n        }\n        // dump($excelData);\n        $this->downloadExcel($excelData,date(\"YmdHis\").\'order\');\n}\nprivate function returnFieldAnnotation(){\n        return [    \n            \'id\' => \'ID\',\n            \'userordernum\' => \'單號\',\n            \'treatment_method\' => \'訂單處理方式\', // 訂單處理方式 1 客戶收錢 2免費單\n            \'client_id\' => \'客名ID\',\n            \'client_name\' => \'客名\',\n            \'client_phone\' => \'客戶電話\',\n            \'receipt_time\' => \'回單日期\',\n            \'out_single_time\' => \'出單日期\',\n            \'delivery_name\' => \'交貨商戶\',\n            \'delivery_phone\' => \'交貨商戶電話\',\n            \'delivery_time\' => \'送貨日期\',\n            // \'cargo_type_id\' => \'\',\n            \'work_type\' => \'工作種類\',\n            \'area_id\' => \'提貨點ID\',\n            \'area_name\' => \'提貨點\',\n            \'address\' => \'地區\',\n            // \'delivery_area_id\' => \'\',\n            \'quantity\' => \'數量\',\n            \'cargo_id\' => \'件/板ID\',\n            \'cargo_name\' => \'件/板\',\n            \'temp\' => \'溫度\',\n            \'cod_chop_chq\' => \'COD/CHOP/CHQ\',\n            \'return_status\' => \'退貨\',\n            \'driver_id\' => \'司機ID\',\n            \'driver_name\' => \'司機\',\n            \'car_id\' => \'車牌ID\',\n            \'car_name\' => \'車牌\',\n            // \'check_personnel_id\' => \'\',\n            \'check_personnel_name\' => \'核對人員\',\n            \'tunnel\' => \'隧道\',\n            \'parking\' => \'停車場\',\n            \'substitute_branch\' => \'代支\',\n            \'maintenance_cost\' => \'維修費\',\n            \'follow_id\' => \'跟車1ID\',\n            \'follow_name\' => \'跟車1\',\n            \'follow_id2\' => \'跟車2ID\',\n            \'follow_name2\' => \'跟車2\',\n            \'driver_ot\' => \'司機OT\',\n            \'follow_ot\' => \'跟車OT\',\n            \'team_id\' => \'TeamID\',\n            \'team_name\' => \'Team\',\n            \'cost\' => \'價錢\',\n            \'status\' => \'訂單進度\',\n            \'input_type\' => \'錄入方式\',\n            \'remarks\' => \'備註\',\n            \'order_type\' => \'訂單類型\',\n            \'liaisons_name\' => \'聯絡人\',\n            \'liaisons_phone\' => \'聯絡電話\',\n            // \'delivery_area_name\' => \'\',\n            // \'delivery_address\' => \'\',\n            // \'unit\' => \'\',\n            // \'note\' => \'\',\n            // \'product_name\' => \'\',\n            // \'store\' => \'\',\n            // \'store_id\' => \'\',\n            // \'price\' => \'\',\n            // \'count\' => \'\',\n            // \'addtime\' => \'\',\n            // \'cargo_type_name\' => \'\',\n            \'sort\' => \'排序\',\n            \'create_time\' => \'創建時間\',\n            \'update_time\' => \'更新時間\',\n            \'delete_time\' => \'刪除時間\'\n        ];\n}\n\n// 导出excel的相关代码\nprotected function downloadExcel(&$date,$fileName){\n        $config = [    \n            \'path\' => $this->getTmpDir() . \'/\',\n        ];\n        // dump($date);die;\n        $fileName = $fileName.\'.xlsx\';\n        $xlsxObject = new \\Vtiful\\Kernel\\Excel($config);\n\n        $xlsxObject->fileName($fileName, \'sheet1\')\n            ->data($date)\n            ->fileName($fileName);\n\n        // Init File\n        // $fileObject = $xlsxObject->fileName($fileName);\n        // Writing data to a file ......\n        // Outptu\n        $filePath = $xlsxObject->output();\n        // dump($filePath);die;\n        // Set Header\n        header(\"Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\");\n        header(\'Content-Disposition: attachment;filename=\"\' . $fileName . \'\"\');\n        header(\'Content-Length: \' . filesize($filePath));\n        header(\'Content-Transfer-Encoding: binary\');\n        header(\'Cache-Control: must-revalidate\');\n        header(\'Cache-Control: max-age=0\');\n        header(\'Pragma: public\');\n        ob_clean();\n        flush();\n        if (copy($filePath, \'php://output\') === false) {    \n            // Throw exception\n        }\n        // Delete temporary file\n        @unlink($filePath);\n}\nprotected function getTmpDir(): string{ \n        $tmp = ini_get(\'upload_tmp_dir\');    \n        if ($tmp !== False && file_exists($tmp)) {\n            return realpath($tmp);    \n        }    \n        return realpath(sys_get_temp_dir());\n}\n \n```codeend\n\n```p\n报错 Fatal error: Allowed memory size of 134217728 bytes exhausted (tried to allocate 20480 bytes) in \n最根本的原因是执行程序的内存空间不够导致内存溢出，可以通过设置ini_set(\'memory_limit\',\'128M\')解决但是这种方式治标不治本，所以必须从释放内存空间和代码优化入手。\n所以查询数据的时候不会一次性查询，而是分开查询分批导入电子表格\n```pend\n\n### 优化后的代码 ###end\n```code\npublic function downloadOrderExcel(){\n        ini_set(\'memory_limit\',\'128M\');\n\n        $get = $this->G();\n        $db = M();\n        // dump($post);die;\n        // file_put_contents(\"test.txt\", \'----\'.json_encode($get).\"\\r\\n\",FILE_APPEND);\n        $search = $this->searchIndex($get);\n        // dump($search);die;\n        \n        // $data = $db->table(\'orders\')->field($field)->where($search[\'where\'])->bind($search[\'bind\'])->findAll();\n\n        \n        $obj =  new form_options;\n        $order_type = $this->conversionArray($obj->getOrderType());\n        $return_status = $this->conversionArray($obj->getReturnStatus());\n\n        $filename = date(\"YmdHis\").\'order\';\n        $xlsxObject = $this->initializationExcel($filename);\n\n        $identification = true;\n        $s = 0;\n        $e = 5000;\n        $field = implode(\',\',array_keys($this->returnFieldAnnotation()));\n        while($identification){\n            // file_put_contents(\"test.txt\", \'0\',FILE_APPEND);\n            if(strpos($search[\'where\'],\'receipt_time\') === false){ \n                $search[\'where\'] .= \' and receipt_time >= ?\';\n                $search[\'bind\'][] = time() - (3600 * 24 * 180);\n            }\n            $data = $db->table(\'orders\')->field($field)->where($search[\'where\'])->bind($search[\'bind\'])->limit($s.\',\'.$e)->findAll();\n            foreach($data as $key => $val){\n                $val[\'receipt_time\'] = $val[\'receipt_time\'] > 0 ? date(\'Y-m-d H:i\',$val[\'receipt_time\']) : \'-\';\n                $val[\'out_single_time\'] = $val[\'out_single_time\'] > 0 ? date(\'Y-m-d H:i\',$val[\'out_single_time\']) : \'-\';\n                $val[\'delivery_time\'] = $val[\'delivery_time\'] > 0 ? date(\'Y-m-d H:i\',$val[\'delivery_time\']) : \'-\';\n                $val[\'order_type\'] = $order_type[$val[\'order_type\']] ?? \'-\';\n                $val[\'return_status\'] = $return_status[$val[\'return_status\']] ?? \'-\';\n        \n                $data[$key] = array_values($val);\n                // $arr = array_values($val);\n                // array_push($excelData,$arr);\n            }\n            if(!$data && $s != 0 ){\n                // 表示已經沒有數據了 推出循環\n                $identification = false;\n                // continue;\n                break;\n            }\n            if($s == 0){\n                // 第一次加上表頭\n                $arr = array_values($this->returnFieldAnnotation());\n                array_unshift($data,$arr);\n            }\n            \n            $s += $e;\n            $xlsxObject = $this->loadDataExcel($xlsxObject,$data);\n            // $data = null;\n            unset($data);\n        }\n        \n        $this->downloadExcel2($xlsxObject,$filename);\n}\n\nprivate function returnFieldAnnotation(){\n        return [    \n            \'id\' => \'ID\',\n            \'userordernum\' => \'單號\',\n            \'treatment_method\' => \'訂單處理方式\', // 訂單處理方式 1 客戶收錢 2免費單\n            \'client_id\' => \'客名ID\',\n            \'client_name\' => \'客名\',\n            \'client_phone\' => \'客戶電話\',\n            \'receipt_time\' => \'回單日期\',\n            \'out_single_time\' => \'出單日期\',\n            \'delivery_name\' => \'交貨商戶\',\n            \'delivery_phone\' => \'交貨商戶電話\',\n            \'delivery_time\' => \'送貨日期\',\n            // \'cargo_type_id\' => \'\',\n            \'work_type\' => \'工作種類\',\n            \'area_id\' => \'提貨點ID\',\n            \'area_name\' => \'提貨點\',\n            \'address\' => \'地區\',\n            // \'delivery_area_id\' => \'\',\n            \'quantity\' => \'數量\',\n            \'cargo_id\' => \'件/板ID\',\n            \'cargo_name\' => \'件/板\',\n            \'temp\' => \'溫度\',\n            \'cod_chop_chq\' => \'COD/CHOP/CHQ\',\n            \'return_status\' => \'退貨\',\n            \'driver_id\' => \'司機ID\',\n            \'driver_name\' => \'司機\',\n            \'car_id\' => \'車牌ID\',\n            \'car_name\' => \'車牌\',\n            // \'check_personnel_id\' => \'\',\n            \'check_personnel_name\' => \'核對人員\',\n            \'tunnel\' => \'隧道\',\n            \'parking\' => \'停車場\',\n            \'substitute_branch\' => \'代支\',\n            \'maintenance_cost\' => \'維修費\',\n            \'follow_id\' => \'跟車1ID\',\n            \'follow_name\' => \'跟車1\',\n            \'follow_id2\' => \'跟車2ID\',\n            \'follow_name2\' => \'跟車2\',\n            \'driver_ot\' => \'司機OT\',\n            \'follow_ot\' => \'跟車OT\',\n            \'team_id\' => \'TeamID\',\n            \'team_name\' => \'Team\',\n            \'cost\' => \'價錢\',\n            \'status\' => \'訂單進度\',\n            \'input_type\' => \'錄入方式\',\n            \'remarks\' => \'備註\',\n            \'order_type\' => \'訂單類型\',\n            \'liaisons_name\' => \'聯絡人\',\n            \'liaisons_phone\' => \'聯絡電話\',\n            // \'delivery_area_name\' => \'\',\n            // \'delivery_address\' => \'\',\n            // \'unit\' => \'\',\n            // \'note\' => \'\',\n            // \'product_name\' => \'\',\n            // \'store\' => \'\',\n            // \'store_id\' => \'\',\n            // \'price\' => \'\',\n            // \'count\' => \'\',\n            // \'addtime\' => \'\',\n            // \'cargo_type_name\' => \'\',\n            \'sort\' => \'排序\',\n            \'create_time\' => \'創建時間\',\n            \'update_time\' => \'更新時間\',\n            \'delete_time\' => \'刪除時間\'\n        ];\n}\n\nprotected function initializationExcel($fileName){\n        $config = [    \n            \'path\' => $this->getTmpDir() . \'/\',\n        ];\n        // dump($date);die;\n        $fileName = $fileName.\'.xlsx\';\n        $xlsxObject = new \\Vtiful\\Kernel\\Excel($config);\n        return $xlsxObject->fileName($fileName, \'sheet1\');\n}\n\nprotected function loadDataExcel($xlsxObject,$data){\n        // file_put_contents(\"test.txt\", \'0-\'.count($data).\'\\r\\n\',FILE_APPEND);\n        return $xlsxObject->data($data);\n}\n\nprotected function downloadExcel2($xlsxObject,$fileName){\n        // $xlsxObject->fileName($fileName);\n\n        // Init File\n        // $fileObject = $xlsxObject->fileName($fileName);\n        // Writing data to a file ......\n        // Outptu\n        $filePath = $xlsxObject->output();\n        // dump($filePath);die;\n        // Set Header\n        header(\"Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet\");\n        header(\'Content-Disposition: attachment;filename=\"\' . $fileName . \'\"\');\n        header(\'Content-Length: \' . filesize($filePath));\n        header(\'Content-Transfer-Encoding: binary\');\n        header(\'Cache-Control: must-revalidate\');\n        header(\'Cache-Control: max-age=0\');\n        header(\'Pragma: public\');\n        ob_clean();\n        flush();\n        if (copy($filePath, \'php://output\') === false) {    \n            // Throw exception\n        }\n        // Delete temporary file\n        @unlink($filePath);\n}\n\nprotected function getTmpDir(): string{ \n        $tmp = ini_get(\'upload_tmp_dir\');    \n        if ($tmp !== False && file_exists($tmp)) {\n            return realpath($tmp);    \n        }    \n        return realpath(sys_get_temp_dir());\n }\n```codeend'),
(40, '回调函数用途', NULL, 'vue 无法上传id的问题', 'yaoxs', '2021-10-22 02:03:57', '2021-11-23 04:04:08', NULL, 1, '```code\n<el-dropdown @command=\"handleCommand\">\n  <span class=\"el-dropdown-link\">\n    下拉菜单<i class=\"el-icon-arrow-down el-icon--right\"></i>\n  </span>\n  <el-dropdown-menu slot=\"dropdown\">\n    <el-dropdown-item command=\"a\">黄金糕</el-dropdown-item>\n    <el-dropdown-item command=\"b\">狮子头</el-dropdown-item>\n    <el-dropdown-item command=\"c\">螺蛳粉</el-dropdown-item>\n  </el-dropdown-menu>\n</el-dropdown>\n\n<style>\n  .el-dropdown-link {\n    cursor: pointer;\n    color: #409EFF;\n  }\n  .el-icon-arrow-down {\n    font-size: 12px;\n  }\n</style>\n\n<script>\n  export default {\n    methods: {\n      handleCommand(command) {\n        this.$message(\'click on item \' + command);\n      }\n    }\n  }\n</script>\n```codeend\n```p\n如果这时候需要把这个下拉列表的事件添加其他参数例如说id之类的，可以利用回调函数添加\n```pend\n```code\n<el-dropdown @command=\"(command) => {handleCommand(scope.row.id,command)}\">\n  <span class=\"el-dropdown-link\">\n    下拉菜单<i class=\"el-icon-arrow-down el-icon--right\"></i>\n  </span>\n  <el-dropdown-menu slot=\"dropdown\">\n    <el-dropdown-item command=\"a\">黄金糕</el-dropdown-item>\n    <el-dropdown-item command=\"b\">狮子头</el-dropdown-item>\n    <el-dropdown-item command=\"c\">螺蛳粉</el-dropdown-item>\n  </el-dropdown-menu>\n</el-dropdown>\n\n<style>\n  .el-dropdown-link {\n    cursor: pointer;\n    color: #409EFF;\n  }\n  .el-icon-arrow-down {\n    font-size: 12px;\n  }\n</style>\n\n<script>\n  export default {\n    methods: {\n      handleCommand(id,command) {\n        this.$message(\'click on item \' + command + id);\n      }\n    }\n  }\n</script>\n```codeend'),
(41, 'PHP获取AUTHORIZATION认证验证', NULL, 'PHP获取AUTHORIZATION认证验证', 'yaoxs', '2021-11-01 01:05:28', '2021-11-23 04:05:59', NULL, 2, '```p\n原文：https://blog.csdn.net/baidu_28393027/article/details/82888299\n```pend\n\n```code\nlet Authorization = localStorage.getItem(\'Authorization\');\n\n// 创建实例\n// https://www.kancloud.cn/yunye/axios/234845\nlet axiosInstance = axios.create({\n    baseURL: API_URL,\n    headers: {\n        \'Content-Type\': \'application/json\',\n        \'X-Requested-With\': \'XMLHttpRequest\',\n        Authorization\n    }\n});\n```codeend\n```p\napache获取HTTP_AUTHORIZATION需要在.htaccess文件中加入\n```pend\n```code\n#Authorization Headers\nRewriteCond %{HTTP:Authorization} ^(.+)$\nRewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]\n```codeend\n\n```code\nif(isset($_SERVER[\'HTTP_ORIGIN\'])){\n    header(\'Access-Control-Allow-Origin:\'.$_SERVER[\'HTTP_ORIGIN\']);\n}\n//header(\'Access-Control-Allow-Origin: *\'); // *代表允许任何网址请求\n// 响应类型\nheader(\'Access-Control-Allow-Methods:*\');\nheader(\'Access-Control-Allow-Headers:x-requested-with,content-type,authorization\'); \nvar_dump($_SERVER[\'HTTP_AUTHORIZATION\']);die;\n```codeend'),
(43, '常用网址', NULL, '', 'yaoxs', '2021-12-13 01:22:14', '2021-12-20 02:22:48', NULL, 1, '```p \nWeb API 接口文档\nhttps://developer.mozilla.org/zh-CN/docs/Web/API/FileReader\n你不知道的 blob\nhttps://www.sohu.com/a/401255256_463987\nDeno bytes 模块全解析\nhttps://mp.weixin.qq.com/s?__biz=MzI2MjcxNTQ0Nw==&mid=2247484317&idx=1&sn=c0b397b6bd5fdfced0c1bebc187a7c0d&chksm=ea47a2c5dd302bd37b285f65dd7a92df8ca1bc213465091e82a28be08ec5808b905e9fb69bec&scene=21#wechat_redirect\n```pend'),
(44, 'JS图片相关的函数以及对象', NULL, '', 'yaoxs', '2021-12-13 01:26:30', '2021-12-13 01:46:34', NULL, 5, '### Image()函数 ###end\n```p \nImage()函数将会创建一个新的HTMLImageElement实例。\n```pend\n\n### HTMLImageElement 对象 ###end\n```p \nHTMLImageElement 接口提供了特别的属性和方法 (在常规的 HTMLElement之外，它也能通过继承使用)来操纵 <img> 元素的布局和图像。\n```pend\n```code\n<script>\n    // Image 构造器\n    var img1 = new Image();\n    img1.src = \'./images/service_06.png\';\n    img1.alt = \'alt\';\n    // 将一个节点附加到指定父节点的子节点列表的末尾处。\n    document.body.appendChild(img1);\n\n    // 使用 DOM HTMLImageElement\n    var img2 = document.createElement(\'img\');\n    img2.src = \'./images/service_07.png\';\n    img2.alt = \'alt text\';\n    document.body.appendChild(img2);\n\n    console.log(document.images);\n</script>\n```codeend'),
(45, 'JS文件操作', NULL, '', 'yaoxs', '2021-12-13 01:54:04', '2021-12-13 04:00:46', NULL, 1, '#### Blob ####end\n```p \nBlob 对象表示一个不可变、原始数据的类文件对象。它的数据可以按文本或二进制的格式进行读取，也可以转换成 ReadableStream 来用于数据操作。 \nBlob 表示的不一定是JavaScript原生格式的数据。File 接口基于Blob，继承了 blob 的功能并将其扩展使其支持用户系统上的文件。\n要从其他非blob对象和数据构造一个 Blob，请使用 Blob() 构造函数。要创建一个 blob 数据的子集 blob，请使用 slice() 方法。要获取用户文件系统上的文件对应的 Blob 对象，请参阅 File 文档。\n```pend\n#### 构造函数 ####end\n```p\nBlob(blobParts[, options])\n返回一个新创建的 Blob 对象，其内容由参数中给定的数组串联组成。\n```pend\n```code\nvar debug1 = {hello1: \"world1\"};\nvar debug2 = {hello2: \"world2\"};\nvar blob = new Blob([JSON.stringify(debug1, null, 2),JSON.stringify(debug2, null, 2)]);\n```codeend\n##### Blob.slice([start[, end[, contentType]]]) #####end\n```p\n返回一个新的 Blob 对象，包含了源 Blob 对象中指定范围内的数据。\n```pend\n```code\nvar debug1 = {hello1: \"world1\"};\nvar debug2 = {hello2: \"world2\"};\nvar blob = new Blob([JSON.stringify(debug1, null, 2),JSON.stringify(debug2, null, 2)]);\nblog.text().then((e) => {console.log(e)});\nlet test = blob.slice(0,20);\ntest.text().then((e) => console.log(e));\n```codeend\n##### 获取文件内容 #####end\n```p\nBlob.slice([start[, end[, contentType]]])\n返回一个新的 Blob 对象，包含了源 Blob 对象中指定范围内的数据。\nBlob.stream()\n返回一个能读取blob内容的 ReadableStream。\nBlob.text()\n返回一个promise且包含blob所有内容的UTF-8格式的 USVString。\nBlob.arrayBuffer()\n返回一个promise且包含blob所有内容的二进制格式的 ArrayBuffer \n```pend\n```p\n\n```pend\n#### File ####end\n```p\nFile()返回一个新构建的文件对象（File）。\n文件（File）接口提供有关文件的信息，并允许网页中的 JavaScript 访问其内容。\n\n通常情况下， File 对象是来自用户在一个 <input> 元素上选择文件后返回的 FileList 对象,也可以是来自由拖放操作生成的 DataTransfer 对象，或者来自 HTMLCanvasElement 上的 mozGetAsFile() API。在Gecko中，特权代码可以创建代表任何本地文件的File对象，而无需用户交互（有关详细信息，请参阅注意事项。\n\nFile 对象是特殊类型的 Blob，且可以用在任意的 Blob 类型的 context 中。比如说， FileReader, URL.createObjectURL(), createImageBitmap() (en-US), 及 XMLHttpRequest.send() 都能处理 Blob 和 File。\n```pend\n##### 属性 #####end\n```p\nFile 接口也继承了 Blob 接口的属性：\n\nFile.lastModified 只读\n返回当前 File 对象所引用文件最后修改时间，自 UNIX 时间起始值（1970年1月1日 00:00:00 UTC）以来的毫秒数。\nFile.lastModifiedDate 只读 \n返回当前 File 对象所引用文件最后修改时间的 Date 对象。\nFile.name 只读\n返回当前 File 对象所引用文件的名字。\nFile.size 只读\n返回文件的大小。\nFile.webkitRelativePath 只读 \n返回 File 相关的 path 或 URL。\nFile.type 只读\n返回文件的 多用途互联网邮件扩展类型（MIME Type）\n```pend\n##### 方法 #####end\n```p\nFile 接口没有定义任何方法，但是它从 Blob 接口继承了以下方法：\nBlob.slice([start[, end[, contentType]]])\n返回一个新的 Blob 对象，它包含有源 Blob 对象中指定范围内的数据。\n```pend\n```p\n\n```pend\n#### FileReader ####end \n```p\nFileReader 对象允许Web应用程序异步读取存储在用户计算机上的文件（或原始数据缓冲区）的内容，使用 File 或 Blob 对象指定要读取的文件或数据。\n\n其中File对象可以是来自用户在一个<input>元素上选择文件后返回的FileList对象,也可以来自拖放操作生成的 DataTransfer对象,还可以是来自在一个HTMLCanvasElement上执行mozGetAsFile()方法后返回结果。\n\n重要提示： FileReader仅用于以安全的方式从用户（远程）系统读取文件内容 它不能用于从文件系统中按路径名简单地读取文件。 要在JavaScript中按路径名读取文件，应使用标准Ajax解决方案进行服务器端文件读取，如果读取跨域，则使用CORS权限。\n```pend\n##### FileReader.readyState属性(只读) #####end\n```p\nEMPTY(0)还没有加载任何数据\nLOADING(1)数据正在被加载\nDONE(2)已完成全部的读取请求\n```pend\n##### FileReader.result 只读 #####end\n```p\n文件的内容。该属性仅在读取操作完成后才有效，数据的格式取决于使用哪个方法来启动读取操作。\n启用读取操作的方法有:FileReader.readAsArrayBuffer(), FileReader.readAsBinaryString(),FileReader.readAsDataURL(),FileReader.readAsText()等\n```pend\n#### 事件处理 ####end\n```p\nFileReader.onabort\n处理abort (en-US)事件。该事件在读取操作被中断时触发。\nFileReader.onerror (en-US)\n处理error (en-US)事件。该事件在读取操作发生错误时触发。\nFileReader.onload\n处理load (en-US)事件。该事件在读取操作完成时触发。\n```pend\n#### 方法 ####end\n##### FileReader.readAsDataURL() 只读 #####end\n```p\n开始读取指定的Blob中的内容。一旦完成，result属性中将包含一个data: URL格式的Base64字符串以表示所读取文件的内容。\n```pend\n ```code \n// 将file转换成img对象 file是一个blob对象\nfunction readImg(file) {\n    return new Promise((resolve, reject) => {\n        const img = new Image()\n        const reader = new FileReader()\n        reader.onload = function(e) {\n           img.src = e.target.result\n        }\n        reader.onerror = function(e) {\n           reject(e)\n        }\n        reader.readAsDataURL(file)\n        img.onload = function() {\n           resolve(img)\n        }\n        img.onerror = function(e) {\n           reject(e)\n        }\n    })\n}\n ```codeend'),
(46, 'Object.defineProperty方法', NULL, '', 'yaoxs', '2021-12-16 02:17:11', '2021-12-16 02:21:50', NULL, 1, '### Object.defineProperty方法 ###end\n```p\n文档: https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty\n\nObject.defineProperty() 方法会直接在一个对象上定义一个新属性，或者修改一个对象的现有属性，并返回此对象。\n语法: Object.defineProperty(obj, prop, descriptor)\nobj 要定义属性的对象。\nprop 要定义或修改的属性的名称或 Symbol 。\ndescriptor 要定义或修改的属性描述符。\n```pend\n### 参数 ###end\n#### writable ####end\n```p\n当且仅当该属性的 writable 键值为 true 时，属性的值，也就是上面的 value，才能被赋值运算符 (en-US)改变。\n默认为 false。\n```pend\n```code\nconst object1 = {};\n\nObject.defineProperty(object1, \'property1\', {\n  value: 42,\n  writable: false\n});\n\nobject1.property1 = 77;\n// throws an error in strict mode\n\nconsole.log(object1.property1);\n// expected output: 42\n```codeend\n#### set ####end\n```p\n属性的 setter 函数，如果没有 setter，则为 undefined。当属性值被修改时，会调用此函数。该方法接受一个参数（也就是被赋予的新值），会传入赋值时的 this 对象。\n默认为 undefined。\n```pend\n```code\n<body>\n    <input type=\"text\" id=\"textInput\">\n    输入：<span id=\"textSpan\"></span>\n    <script>\n        var obj = {},\n            textInput = document.querySelector(\'#textInput\'),\n            textSpan = document.querySelector(\'#textSpan\');\n\n        Object.defineProperty(obj, \'foo\', {\n            set: function (newValue) {\n            textInput.value = newValue;\n            textSpan.innerHTML = newValue;\n            }\n        });\n\n        textInput.addEventListener(\'keyup\', function (e) {\n            obj.foo = e.target.value;\n        });\n\n    </script>\n</body>\n```codeend'),
(50, 'mysql常用sql', NULL, '', 'yaoxs', '2022-02-28 01:59:00', '2022-03-02 02:00:28', NULL, 6, '### 設置字段的默認值 ###end \n ```code\n# 修改數據表字段\nalter table jal_info alter column update_time set default 0;\n# 添加數據表字段\nalter table orders add pickup_merchant_phone varchar(255) NOt NULL default \'\' COMMENT \'提貨商戶電話\';\n ```codeend');

-- --------------------------------------------------------

--
-- 表的结构 `article_category`
--

CREATE TABLE `article_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '类别标题',
  `desc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '类别描述',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 转存表中的数据 `article_category`
--

INSERT INTO `article_category` (`id`, `title`, `desc`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '随笔', NULL, '2021-07-13 07:56:36', '2021-07-17 20:37:01', NULL),
(2, 'PHP', NULL, '2021-07-15 23:24:26', '2021-07-15 23:24:26', NULL),
(3, 'HTML', NULL, '2021-07-15 23:43:47', '2021-07-15 23:43:47', NULL),
(4, 'CSS', NULL, '2021-07-15 23:43:57', '2021-07-15 23:43:57', NULL),
(5, 'JavaScript', NULL, '2021-07-15 23:44:09', '2021-07-15 23:44:09', NULL),
(6, 'MYSQL', NULL, '2021-07-15 23:44:22', '2021-07-15 23:44:22', NULL),
(7, 'Redis', NULL, '2021-07-15 23:44:34', '2021-07-15 23:44:34', NULL),
(15, 'test222', NULL, '2021-08-01 23:33:44', '2021-08-06 06:09:36', '2021-08-06 06:09:36'),
(16, 'test7', NULL, '2021-08-01 23:37:26', '2021-08-06 06:08:15', '2021-08-06 06:08:15'),
(17, '随笔22', NULL, '2021-08-01 23:39:38', '2021-08-02 00:27:43', '2021-08-02 00:27:43'),
(18, 'PHP44', NULL, '2021-08-01 23:40:32', '2021-08-02 01:24:24', '2021-08-02 01:24:24'),
(19, 'HTML2313', NULL, '2021-08-02 01:24:32', '2021-08-02 01:25:47', '2021-08-02 01:25:47'),
(20, 'H', NULL, '2021-08-02 01:25:56', '2021-08-02 01:35:40', '2021-08-02 01:35:40'),
(22, 'test22', NULL, '2021-08-06 06:13:08', '2021-08-06 06:13:35', '2021-08-06 06:13:35'),
(23, 'test', NULL, '2021-08-06 07:59:24', '2021-08-06 07:59:27', '2021-08-06 07:59:27'),
(24, 'test', NULL, '2021-08-06 08:03:00', '2021-08-06 08:03:03', '2021-08-06 08:03:03'),
(25, 'test', NULL, '2021-08-06 08:04:09', '2021-08-06 08:04:18', '2021-08-06 08:04:18'),
(26, 'test2', NULL, '2021-08-08 01:42:27', '2021-08-08 01:44:43', '2021-08-08 01:44:43'),
(27, 'VUE', NULL, '2021-10-13 19:25:03', '2021-10-13 19:25:03', NULL),
(28, 'test', NULL, '2021-10-21 02:04:04', '2021-10-21 02:04:09', '2021-10-21 02:04:09'),
(29, 'test1', NULL, '2021-10-21 02:10:01', '2021-10-21 02:20:07', '2021-10-21 02:20:07');

-- --------------------------------------------------------

--
-- 表的结构 `article_comment`
--

CREATE TABLE `article_comment` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `article_id` bigint(20) UNSIGNED NOT NULL COMMENT '文章id',
  `comment_user_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '评论用户（暂定游客或者作者）',
  `comment_content` text COLLATE utf8mb4_unicode_ci COMMENT '评论内容',
  `enable` tinyint(1) DEFAULT '0' COMMENT '是否启用 0 否 1是',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 转存表中的数据 `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(2, '2014_10_12_000000_create_users_table', 2),
(3, '2014_10_12_200000_add_two_factor_columns_to_users_table', 2),
(4, '2021_07_07_072700_create_sessions_table', 2),
(12, '2014_10_12_100000_create_password_resets_table', 3),
(13, '2016_06_01_000001_create_oauth_auth_codes_table', 3),
(14, '2016_06_01_000002_create_oauth_access_tokens_table', 3),
(15, '2016_06_01_000003_create_oauth_refresh_tokens_table', 3),
(16, '2016_06_01_000004_create_oauth_clients_table', 3),
(17, '2016_06_01_000005_create_oauth_personal_access_clients_table', 3),
(18, '2019_08_19_000000_create_failed_jobs_table', 3);

-- --------------------------------------------------------

--
-- 表的结构 `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 转存表中的数据 `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('103345da5ebbd1cfbe10e2c31afc4c4b00d4044a23a87bbf1f06954d9d5cf4a0e3e6ecda44332b2f', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-29 01:03:28', '2021-07-29 01:03:28', '2021-08-05 09:03:28'),
('1376c7dfe39f22dfd85d5be01c12be2bbbba62b68797f79b10afd0ba4249c9c435aeffe8357eab7d', 1, '3', 'Personal Access Token', '[]', 1, '2021-07-31 23:45:44', '2021-07-31 23:45:44', '2022-08-01 07:45:44'),
('139608a4f7f0846c2cc1b852bc08deaa72c3d5fb438eddb2c2e981b63bb8f0c11b4c251b88e4535e', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-28 23:11:40', '2021-07-28 23:11:40', '2021-08-05 07:11:40'),
('1699666aa6b9c624ed0ba2d6f7aec773886198f2697e54190835853cb2a957118bd22de0d8954dc6', 1, '9403', 'Personal Access Token', '[]', 0, '2021-07-30 01:39:12', '2021-07-30 01:39:12', '2022-07-30 09:39:12'),
('1861cd37320d13682426780ce34db3391cfcb645ec66ef14aaf406233ef0d3bb3dd18bdb8640018c', 1, '3', 'Personal Access Token', '[]', 0, '2021-07-31 23:23:45', '2021-07-31 23:23:45', '2022-08-01 07:23:45'),
('1fd9ca498d64faca80736c5c39046524054184d4a9ef6a57379bc13a42b03a5a46777759f3182e01', 1, '3', 'Personal Access Token', '[]', 0, '2021-10-14 19:14:20', '2021-10-14 19:14:20', '2022-10-15 03:14:20'),
('2ab424d607765bfc9a8cdb61a667c2df628b1cd36cb558a88a0e26c26c4d225463e760af8e42c34e', 1, '3', 'Personal Access Token', '[]', 0, '2021-10-21 01:09:37', '2021-10-21 01:09:37', '2022-10-21 09:09:37'),
('31937c3df5d8950eede8b46745a9cbf016ecf083c8e298209d2199e7da32d66bf20c6262b0bf0930', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-28 23:09:56', '2021-07-28 23:09:56', '2021-08-05 07:09:56'),
('36d5bfeec18b3aa0f057d86de900decd9f0417f5e142c444eaeef619cd30dce77451673ca45a1598', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-28 23:02:48', '2021-07-28 23:02:48', '2021-08-05 07:02:48'),
('37717ad887d0d2e2fa501753318d4cc67b6661affedaf7eb4ebcb1f7f2fe854f86ed6d6151c56a80', 1, '3', 'Personal Access Token', '[]', 1, '2021-10-20 19:12:40', '2021-10-20 19:12:40', '2022-10-21 03:12:40'),
('3896136a32b4a50833c21fa7c79894359aa31b744fefa2b759d832c13b9bf3305bd4f2be9feffb4e', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-28 23:02:08', '2021-07-28 23:02:08', '2021-08-05 07:02:09'),
('39f650bc54b08af584e16d225d7dc7b9012b66b6da6846c93fba8333e4020f5d4fe625ba36fdf106', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-28 23:12:24', '2021-07-28 23:12:24', '2021-08-05 07:12:25'),
('3d72fdeb5bee39dd09540a83c3cc004ea9f2c781db0c1921d096186dc267c0c87cb30715e5e980da', 1, '3', 'Personal Access Token', '[]', 1, '2021-10-14 19:14:20', '2021-10-14 19:14:20', '2022-10-15 03:14:20'),
('49b6fa65e64ac3ebd971ba7702c99613716570fa24075045f63eccbf253ca4768758412c2f1a382c', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-28 23:12:14', '2021-07-28 23:12:14', '2021-08-05 07:12:14'),
('4a5aefa492e684c12fff2872100b90580e1408118797054ef2475abd1259f2ae6dfee6ec9b3e946d', 1, '3', 'Personal Access Token', '[]', 0, '2021-08-01 19:10:55', '2021-08-01 19:10:55', '2022-08-02 03:10:55'),
('4cb289a2c9251cf3a78b4da372a106029110ee5161dbcb1af3f7766618cc13e93a882499013d89d7', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-28 23:11:33', '2021-07-28 23:11:33', '2021-08-05 07:11:33'),
('4f404337fc28479e4fb6bdf9476ce010f0dcca6ef8879c61b26e42f36509446491e4c0e013644d46', 1, '9403', 'Personal Access Token', '[]', 0, '2021-07-30 01:40:03', '2021-07-30 01:40:03', '2022-07-30 09:40:03'),
('50fb5b3d461cb962ab702002b0ad868e5c6e0dae46c946c3d2a7300f36212f5de9a46062e7448769', 1, '3', 'Personal Access Token', '[]', 0, '2021-07-31 23:32:09', '2021-07-31 23:32:09', '2022-08-01 07:32:09'),
('52fdab58aa6f458edc6943dd8d68a188c1bcbaf07085c7fc0c2b65e329b474790c9e15881110e95c', 1, '3', 'Personal Access Token', '[]', 0, '2021-10-14 19:14:20', '2021-10-14 19:14:20', '2022-10-15 03:14:20'),
('53a0f18e3ef82a32a1f1a7130b0621ec34528148b4991b665b1b905f33130c2a358d0a4b3cdee34f', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-27 19:34:05', '2021-07-27 19:34:05', '2021-08-04 03:34:06'),
('55448c629444bb861e08879d59070b0ddc30cfaa02476da26906a5cef16c82a4489e631e60d55fed', 1, '3', 'Personal Access Token', '[]', 0, '2021-10-14 19:14:23', '2021-10-14 19:14:23', '2022-10-15 03:14:23'),
('55b981e87afc155d20802b58044fa08eaa7c61254b79ce93eb065d860ba1af2ec40902444acca95d', 1, '3', 'Personal Access Token', '[]', 1, '2021-10-14 00:16:39', '2021-10-14 00:16:39', '2022-10-14 08:16:39'),
('5688a5fa5f84935304ae982c29fdf3da1cef8b9c72c4577bd734bea146f31d6fa07e0ae367655aaf', 1, '3', 'Personal Access Token', '[]', 0, '2021-07-31 23:16:38', '2021-07-31 23:16:38', '2022-08-01 07:16:38'),
('5697e72f86f161098f28957ed5145ae5f77fafccca7254b9ce84d8fc76540594b99e9a2619c74fad', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-30 00:06:05', '2021-07-30 00:06:05', '2021-08-06 08:06:05'),
('572c1e951e3f18d0f20e699f27de0e382bd43bacf62969d865e1a58da2a2e0c9f327f231ca7d6459', 1, '3', 'Personal Access Token', '[]', 0, '2021-10-14 19:14:21', '2021-10-14 19:14:21', '2022-10-15 03:14:21'),
('58c0d95c2fcbf1ed65b4d2f136e35e7dfec64dda7fe320b54f9a4f39e6ec4b42cacb0e6e93480d44', 1, '3', 'Personal Access Token', '[]', 1, '2021-10-14 00:19:57', '2021-10-14 00:19:57', '2022-10-14 08:19:57'),
('5efda2569d699351e8287a57ca4ee2c5af24398248a3d9cbff470c6916441e20b62d0886669e23c3', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-28 23:02:41', '2021-07-28 23:02:41', '2021-08-05 07:02:41'),
('617ae41f25ae3eb5ddd101e02a996d449d97a738547f02ae3f027937d2cf3588d2ef3b29201a8ed3', 1, '9403', 'Personal Access Token', '[]', 0, '2021-07-30 01:39:32', '2021-07-30 01:39:32', '2022-07-30 09:39:32'),
('65223b5f3eeb83715ab22c436579fd721f88cf9f8cb9d0a602756c9f7cf326c0d92b62fb4c9bba4d', 1, '3', 'Personal Access Token', '[]', 1, '2021-10-14 00:19:25', '2021-10-14 00:19:25', '2022-10-14 08:19:25'),
('6b257199fca236525e2e7bbd3e68ef542eb32f68debf66ba6d6768d6f8cea8cf17733f957891a988', 1, '3', 'Personal Access Token', '[]', 0, '2021-07-30 01:48:53', '2021-07-30 01:48:53', '2022-07-30 09:48:53'),
('6da50d322eda0cc56768c9a0f9f73bc735505fe43ceb213f215033d56bb284e853b00660e035ea9f', 1, '3', 'Personal Access Token', '[]', 0, '2021-07-31 23:32:18', '2021-07-31 23:32:18', '2022-08-01 07:32:18'),
('6f05672a91c9167df2ba2ee0724438ac22de0c82376a755bed6baacdeadf14011e8fe5d0d91202bc', 1, '3', 'Personal Access Token', '[]', 0, '2021-07-31 23:16:35', '2021-07-31 23:16:35', '2022-08-01 07:16:35'),
('75d7c3b20845dac8da0d9fa6972446e602d09a8189065d8b7e2de449e4bedc3e00b2ac013718f879', 1, '3', 'Personal Access Token', '[]', 0, '2021-07-31 23:24:15', '2021-07-31 23:24:15', '2022-08-01 07:24:15'),
('77f58362a56888316bc7a9007db4854f618a1c9f54df6517b3639d38956a34231beb87b2828dc804', 1, '9403', 'Personal Access Token', '[]', 0, '2021-07-30 01:33:52', '2021-07-30 01:33:52', '2022-07-30 09:33:52'),
('79e22e2566fc9c1fdc241ae05b0d109fe56aab599066bec37071ce999afd16c5028c7b1928adec8b', 1, '9403', 'Personal Access Token', '[]', 0, '2021-07-30 01:34:05', '2021-07-30 01:34:05', '2022-07-30 09:34:05'),
('7fe069445d71bbc6019a48112dcd05d3d33fc474d7327b26ff2fab8a0e8ff336818e59a6b2647b88', 1, '9403', 'Personal Access Token', '[]', 0, '2021-07-30 01:33:49', '2021-07-30 01:33:49', '2022-07-30 09:33:49'),
('815ff7f4f492bd3d549c0751f13ea048223c690088b262fc299fd303830ae5e38e7f4eb67cd74273', 1, '3', 'Personal Access Token', '[]', 1, '2021-10-14 18:07:19', '2021-10-14 18:07:19', '2022-10-15 02:07:19'),
('8e4bb137e284a50e95b44cc6dc3c3e56d7a6786eafa2d1e4445323d6a2a0e19e438bd1a0512beba8', 1, '3', 'Personal Access Token', '[]', 1, '2021-10-14 17:40:42', '2021-10-14 17:40:42', '2022-10-15 01:40:42'),
('8f36d6cba07f2a6ef4b1eb168b9fa6be9024177d1fed5d7c1320e2d00d3ddd02bf2eb5e53844f191', 1, '3', 'Personal Access Token', '[]', 1, '2021-10-29 02:04:06', '2021-10-29 02:04:06', '2022-10-29 10:04:06'),
('9163a0083c4ddfa7fee9d1e80ef137facc0e5606cb3b36b91fc5387e4bd06e030aeea16ef6224f8a', 1, '3', 'Personal Access Token', '[]', 1, '2021-10-11 02:24:10', '2021-10-11 02:24:10', '2022-10-11 10:24:10'),
('95c4a1155ed14871351dc261e90068bde5d396e9325f1c796d1bc007d06709c0b52f008e0717fa6b', 1, '9403', 'Personal Access Token', '[]', 0, '2021-07-30 01:14:40', '2021-07-30 01:14:40', '2022-07-30 09:14:40'),
('96cbe830e9ab9054391cd6f9dc4bde2ca0d81ed3a148da9757dd56e073de0143c870b00f2dc97e0f', 1, '3', 'Personal Access Token', '[]', 0, '2021-10-20 19:12:40', '2021-10-20 19:12:40', '2022-10-21 03:12:40'),
('971ed11429513ee1970bcec77bd92c3a9ee06a366e2fadc8507b497b76fd03fc1bb8dce35b8a744e', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-29 01:03:33', '2021-07-29 01:03:33', '2021-08-05 09:03:33'),
('9a430f8d602924d234c98faed185b1583d6d8fa5e4fa0ad6ecba951273eb3d307163207b47a992f3', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-27 19:40:43', '2021-07-27 19:40:43', '2021-08-04 03:40:43'),
('9bdd6cdefb88dd7904a8a15744c98c9e66e0c9699965a34d8017b36f8eb65027f5f7c8c9ae2e197e', 1, '3', 'Personal Access Token', '[]', 1, '2021-08-01 01:26:55', '2021-08-01 01:26:55', '2022-08-01 09:26:55'),
('a3864a75a97686b4126fceac2ec11b5195ede80fb3d79bba3c7d13d742c9d3a3df26a138aa6aec6d', 1, '3', 'Personal Access Token', '[]', 0, '2021-10-14 19:14:23', '2021-10-14 19:14:23', '2022-10-15 03:14:23'),
('a982e1e174e1417519dc57b1183f64347a0a6498aa3e9a1a5d9ad94e769553ac0041b686a2bd6ab9', 1, '9403', 'Personal Access Token', '[]', 0, '2021-07-30 01:14:34', '2021-07-30 01:14:34', '2022-07-30 09:14:34'),
('a9e78cfcd1b61e7285c4bc3af1813fc6062361fc30a808601d03664ee5ec689aa84ce03bf5a0952d', 1, '3', 'Personal Access Token', '[]', 0, '2021-07-31 23:16:58', '2021-07-31 23:16:58', '2022-08-01 07:16:58'),
('aae8939f5af2980d8186e2de9439c207916a5663b5581e5adabc6dfcd45717d916af8f19fa925f96', 1, '3', 'Personal Access Token', '[]', 0, '2021-10-14 18:07:48', '2021-10-14 18:07:48', '2022-10-15 02:07:48'),
('af64b52a32b464a2c41d6deece0ce548242d1f6abd8c909e19fe9aaded0f92a6151d720e9eab4b80', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-29 01:17:29', '2021-07-29 01:17:29', '2021-08-05 09:17:30'),
('b3822efb6c1372dd576faf45b16639d5df38e1495afb8cd2b9e9bb2514d3b132404566f560237c3d', 1, '3', 'Personal Access Token', '[]', 0, '2021-07-30 01:48:51', '2021-07-30 01:48:51', '2022-07-30 09:48:51'),
('b4e21e560f18be7a3bf76db404fff991b393b6c3896dfc1cd882be5379a89da7e7192c8c429dde1c', 1, '3', 'Personal Access Token', '[]', 1, '2021-07-31 23:32:24', '2021-07-31 23:32:24', '2022-08-01 07:32:24'),
('b58d21a50cb77136a46ee4aeee03c6f61e667f61c46766fec18b6f3ed4562c8da7f4f9a9746ea688', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-28 23:11:35', '2021-07-28 23:11:35', '2021-08-05 07:11:35'),
('b5db1e2aa68b4eb69911ee6788b6bd01954160d8382c5297d0c5d3f2ac0ac2e875564bf57c7d2779', 1, '9403', 'Personal Access Token', '[]', 0, '2021-07-30 01:39:59', '2021-07-30 01:39:59', '2022-07-30 09:39:59'),
('b738d8408a71beebebaa34aa8d2824308dcedbd2fba1f923a5750795fb5ff37efdefa1d84672f89e', 1, '3', 'Personal Access Token', '[]', 0, '2021-07-31 23:24:00', '2021-07-31 23:24:00', '2022-08-01 07:24:00'),
('bafca2895030360c2da3b3c04cbe608aeb46650fcd6508c5921a710f7faa6ecc30e90603da8fb06e', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-28 23:11:29', '2021-07-28 23:11:29', '2021-08-05 07:11:29'),
('bc6bb050a187c723d45fa78a7ca27f59250ea825fba77122ecdf7182035525b84f3670408343db46', 1, '3', 'Personal Access Token', '[]', 0, '2021-10-14 19:14:20', '2021-10-14 19:14:20', '2022-10-15 03:14:20'),
('c037e93ebdcc884ed3b1f029f5e97124e3562dd62f85bcd6cc3bd97ee8b65201e5c3cb0b555acadb', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-27 19:51:33', '2021-07-27 19:51:33', '2021-08-04 03:51:34'),
('c1c324f21231d90fd3dc3128279808c91a232e68617b00b4484a21327145238c4dc0ca697748ab12', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-29 01:03:35', '2021-07-29 01:03:35', '2021-08-05 09:03:36'),
('c3d2f71aeffb68c51f1f9e33bb71d566f929abc5c72fa8ac5d4baba286417ddebf0773f3be0fa9b8', 1, '9403', 'Personal Access Token', '[]', 0, '2021-07-30 01:14:38', '2021-07-30 01:14:38', '2022-07-30 09:14:38'),
('c59b5e2ea08248e8ec620d5f3e53fc0c88a747f20e41d9a9111b19346e588b71f77bca2fcee2d274', 1, '3', 'Personal Access Token', '[]', 0, '2021-10-04 02:15:48', '2021-10-04 02:15:48', '2022-10-04 10:15:48'),
('c70779d0bb2773b1f15fb6d1caa36cb1cf3221793d40ce8323cfec73f69d060698b4bc52e4d02a3a', 1, '3', 'Personal Access Token', '[]', 0, '2021-07-31 23:22:14', '2021-07-31 23:22:14', '2022-08-01 07:22:14'),
('cbd70762df9651470e14aca83cb1808414b23b3fba30b038982959eff295e19bfe9223b36a18e4af', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-27 20:10:47', '2021-07-27 20:10:47', '2021-08-04 04:10:47'),
('cdcea7e9c6a6f6d796f05c1a10260e9b096b9457afd5319bfac9320953b4f618fd6a282407c07947', 1, '3', 'Personal Access Token', '[]', 1, '2021-10-14 18:07:48', '2021-10-14 18:07:48', '2022-10-15 02:07:48'),
('cec15a3516345942242bcb2d0a7669d78b9417c87dca33b6125e95515baf946e93264b7dde793e99', 1, '3', 'Personal Access Token', '[]', 1, '2021-10-14 17:51:43', '2021-10-14 17:51:43', '2022-10-15 01:51:43'),
('d15c62aae71cd655096ecdd8c346f6ab994471be1a0c3328ad3ba90d716c180045376a1091593824', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-28 23:12:34', '2021-07-28 23:12:34', '2021-08-05 07:12:34'),
('d3fcf4a9455e6c668582dd136c9f5671906950de3b02788e9a99dd13cc7cd868d5e1980a8f4856e5', 1, '3', 'Personal Access Token', '[]', 1, '2021-08-01 00:04:37', '2021-08-01 00:04:37', '2022-08-01 08:04:37'),
('d71e346ca10ce241fa8b189cc6529866a26b5e487f876289fee4fcae6e3b17be4df41b39509a0cdc', 1, '3', 'Personal Access Token', '[]', 0, '2021-07-31 23:16:06', '2021-07-31 23:16:06', '2022-08-01 07:16:06'),
('d7778d52e845cd0357b58df29d4a28b5ef95a481d8f308692128afcb6203963baf09dc94f99ddb64', 1, '3', 'Personal Access Token', '[]', 0, '2021-08-01 01:28:21', '2021-08-01 01:28:21', '2022-08-01 09:28:21'),
('de01cb70190c93ef224978ead9ac7e549bc343077a5012ca738069c078f3fc26f88559ac36b80270', 1, '3', 'Personal Access Token', '[]', 1, '2021-08-14 04:21:55', '2021-08-14 04:21:55', '2022-08-14 12:21:55'),
('e7a469e0a16af86a2a369c56608c54c19a771fd61d07fd7c0f2c37b999f7f59ff78734044beb65cc', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-29 01:03:40', '2021-07-29 01:03:40', '2021-08-05 09:03:40'),
('ea94c4a394218f5cdece5e8905f42b7ad0267532f1b6f148c660ab6602178ecc071259aef69cdb30', 1, '3', 'Personal Access Token', '[]', 1, '2021-10-14 18:02:50', '2021-10-14 18:02:50', '2022-10-15 02:02:50'),
('eaa9ac9ca9473e710383322d67573ae641a37d924c2b25ee2f11deaeb06d9173eeda29b2b8a1f4d7', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-28 23:11:09', '2021-07-28 23:11:09', '2021-08-05 07:11:09'),
('ed8a976e768570ccef239cf33c60b40a3f639fbdbf230032897d09ba7bece278359d20b7e378b72d', 1, '9403dd47-875f-40b8-a710-a7b252d881d2', 'Personal Access Token', '[]', 0, '2021-07-29 17:29:57', '2021-07-29 17:29:57', '2021-08-06 01:29:58'),
('f1af2220514e184edad7886513de39abdc0a536b8c87b87dd353141c7b22934143a89f918fb09b58', 1, '3', 'Personal Access Token', '[]', 0, '2021-10-14 19:14:20', '2021-10-14 19:14:20', '2022-10-15 03:14:20'),
('f3d6f00e4aa614f5d68465b130b411b867d0320a46cd01ad793249a1893ae889a2d384c7d6fde14b', 1, '3', 'Personal Access Token', '[]', 0, '2021-10-31 23:50:49', '2021-10-31 23:50:49', '2022-11-01 07:50:49'),
('f67ff866a6a504b56220d762fef4b43c0885e5ba532862b2bbdb16441e1a2725a83ef252c1d99943', 1, '3', 'Personal Access Token', '[]', 1, '2021-08-01 00:01:58', '2021-08-01 00:01:58', '2022-08-01 08:01:58'),
('f996f3860cab5fb15ed917dfd315c84fd94dfd88820dab382d44cacd0a89a135022cf1563cbeeffd', 1, '3', 'Personal Access Token', '[]', 0, '2021-07-31 23:29:39', '2021-07-31 23:29:39', '2022-08-01 07:29:39');

-- --------------------------------------------------------

--
-- 表的结构 `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 转存表中的数据 `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(3, NULL, 'Laravel Personal Access Client', 'syoPPQj24KfaBFEjvR3f6gJWhE8PZ7R7yPp3Cluv', NULL, 'http://localhost', 1, 0, 0, '2021-07-30 01:48:20', '2021-07-30 01:48:20'),
(4, NULL, 'Laravel Password Grant Client', '3iSz54MIyPW0EBUANDXnQSsJEIvFKBiYKTk5rl7i', 'users', 'http://localhost', 0, 1, 0, '2021-07-30 01:48:21', '2021-07-30 01:48:21');

-- --------------------------------------------------------

--
-- 表的结构 `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 转存表中的数据 `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, '9403dd47-875f-40b8-a710-a7b252d881d2', '2021-07-27 19:33:02', '2021-07-27 19:33:02'),
(2, '3', '2021-07-30 01:48:20', '2021-07-30 01:48:20');

-- --------------------------------------------------------

--
-- 表的结构 `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- 表的结构 `sessions`
--

CREATE TABLE `sessions` (
  `id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 转存表中的数据 `sessions`
--

INSERT INTO `sessions` (`id`, `user_id`, `ip_address`, `user_agent`, `payload`, `last_activity`) VALUES
('3ucaeNPv2AiBXyKcYUO7SjQcNJIa2U4KkEOGwJhO', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoib3lQY1J3YkRud210djFqOWJaMlRWVnd6UTQ2NE1xWjl5S1JVV0VuVCI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTU6Imh0dHA6Ly9ibG9nLmNvbSI7fX0=', 1626840486),
('8LoZzUw0nwS79jXIghc4ztOc496nKK1U7vdNzYW5', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36', 'YToyOntzOjY6Il90b2tlbiI7czo0MDoiR0xWa2RTOXJMOExHYjFaUFFvN1N0MGR1M2w2dk04NTBOeEdzcGkyQSI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1626781407),
('LyLNWyrFeid6tGZnoA9R3Hcnn2ep9RrQzeAaQnFJ', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.164 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiMkFUUGhBT3RGSWI1dHF3aktvOWs4aG96VnRVbXk2YkNabjVWaVVHTiI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTU6Imh0dHA6Ly9ibG9nLmNvbSI7fX0=', 1626774187),
('PwJ5uMIbb3VgtVdEgHHNexvsDEv1jm4QAR2D56lX', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoidUFRWjNrVnRGbXU5OHdKM1hZR1ZPVGZuOTJDRWtqUlZNUWQzRG5jRiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTM6Imh0dHA6Ly90dC5jb20iO31zOjY6Il9mbGFzaCI7YToyOntzOjM6Im9sZCI7YTowOnt9czozOiJuZXciO2E6MDp7fX19', 1626784927),
('qI2n0o7hup0roIaVld1UqKiKziB5xyWriYmBJgLI', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'YToyOntzOjY6Il90b2tlbiI7czo0MDoiaFFrWnQ3WWFMc0R3NWV1TlRKeXhrdjNsbjdTOWJVQUd3VG9LSkdXVyI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319fQ==', 1626858167),
('sxEsmnwxOaVXJQvkhADFPPHZuPvI7sZcbBM3mcHa', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoicFBHVW1NR1ZmVFVGT0tZdnMwQ2R2TnJMcVZWSzAzUlk1RExmR05hNyI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTU6Imh0dHA6Ly9ibG9nLmNvbSI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1626794448),
('uhQw5fgqsS789cMHVSRHTHG0SOOvyLosdcruq5Ai', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.164 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiREZ1a1ZMcWdwVFJib1E5WUVEejB0RllCU25KaXFiTk9GRGRQcTRBeCI7czo2OiJfZmxhc2giO2E6Mjp7czozOiJvbGQiO2E6MDp7fXM6MzoibmV3IjthOjA6e319czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTU6Imh0dHA6Ly9ibG9nLmNvbSI7fX0=', 1626755291),
('vmchpTpsXtcdvzse19CzKpiKww20aIbFnYRxsUMb', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoibm5FS1Y4MEtWVDdtVWl3Mlp6aXlWVlV5QUdwMHlmMGwzVTV5ZERzeSI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6NDU6Imh0dHA6Ly90dC5jb20vaW5kZXgucGhwL2FkbWluL2FydGljbGVDYXRlZ29yeSI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1627805193),
('XnHGZU2hLPSoKSNWpFQKsvKEoKOuM7iA4PjzbbCR', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiM0dldHkwYVJxOEZlZ3VxYmxCTkhzMUJadWdiWWFhVkUxZkVmN3JnRCI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTU6Imh0dHA6Ly9ibG9nLmNvbSI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1626781407),
('Y05ARd2YCwaOAFFR9LFAKBavGEkww4fSdbQ5hvqd', NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36', 'YTozOntzOjY6Il90b2tlbiI7czo0MDoiY2pia3paUENCNHRFVUhOODRpU2l2SUgzdm1KOGREUndZRGNuaHE4YiI7czo5OiJfcHJldmlvdXMiO2E6MTp7czozOiJ1cmwiO3M6MTI6Imh0dHA6Ly90LmNvbSI7fXM6NjoiX2ZsYXNoIjthOjI6e3M6Mzoib2xkIjthOjA6e31zOjM6Im5ldyI7YTowOnt9fX0=', 1626782148);

-- --------------------------------------------------------

--
-- 表的结构 `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `two_factor_secret` text COLLATE utf8mb4_unicode_ci,
  `two_factor_recovery_codes` text COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `current_team_id` bigint(20) UNSIGNED DEFAULT NULL,
  `profile_photo_path` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- 转存表中的数据 `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `two_factor_secret`, `two_factor_recovery_codes`, `remember_token`, `current_team_id`, `profile_photo_path`, `created_at`, `updated_at`) VALUES
(1, 'yaoxs', '980886119@qq.com', NULL, '$2y$10$fJtVrzUV447k8G.xmtBslOTZqgS.ZJ54PwT0pHNuPJ1axc2.z/SXO', NULL, NULL, NULL, NULL, NULL, '2021-07-09 01:23:03', '2021-07-09 01:23:03');

--
-- 转储表的索引
--

--
-- 表的索引 `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `article_category`
--
ALTER TABLE `article_category`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `article_comment`
--
ALTER TABLE `article_comment`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- 表的索引 `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- 表的索引 `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- 表的索引 `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- 表的索引 `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- 表的索引 `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- 表的索引 `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- 表的索引 `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_user_id_index` (`user_id`),
  ADD KEY `sessions_last_activity_index` (`last_activity`);

--
-- 表的索引 `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `article`
--
ALTER TABLE `article`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- 使用表AUTO_INCREMENT `article_category`
--
ALTER TABLE `article_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- 使用表AUTO_INCREMENT `article_comment`
--
ALTER TABLE `article_comment`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- 使用表AUTO_INCREMENT `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- 使用表AUTO_INCREMENT `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
