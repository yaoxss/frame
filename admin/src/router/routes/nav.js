import indexView from '@/view/index/index.vue';
import categoryView from '@/view/index/category/index.vue';
import categoryCreateView from '@/view/index/category/create.vue';
import categoryEditView from '@/view/index/category/edit.vue';
import articleView from '@/view/index/article/index.vue';
import articleCreateView from '@/view/index/article/create.vue';
import articleEditView from '@/view/index/article/edit.vue';

const category = [
    {
        path: 'category',
        component: categoryView,
        navName: '分类管理',
        isShow: true
    },
    {
        path: 'category/create',
        component: categoryCreateView,
        navName: '创建分类',
        isShow: false
    },
    {
        path: 'category/edit/:id',
        component: categoryEditView,
        navName: '編輯分类',
        props: true,
        isShow: false
    },
];

const article = [
    {
        path: 'article',
        component: articleView,
        navName: '文章管理',
        isShow: true
    },
    {
        path: 'article/create',
        component: articleCreateView,
        navName: '创建文章',
        isShow: false
    },
    {
        path: 'article/edit/:id',
        component: articleEditView,
        navName: '編輯文章',
        props: true,
        isShow: false
    },
];

const nav = {
    path: '/index',
    component: indexView,
    children: [
        ...category,
        ...article
    ]
}

export default nav;