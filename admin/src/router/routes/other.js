import loginView from '@/view/login.vue';

const login = {
    path: '/',
    name: 'login',
    component: loginView
}

const routes = [
    login
];

export default routes;