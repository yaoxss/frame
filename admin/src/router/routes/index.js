import navRouter from '@/router/routes/nav.js';
import otherRouter from '@/router/routes/other.js';

const nav = [navRouter];
const other = otherRouter;

const routes = other.concat(nav);

export default routes;