import {createRouter,createWebHashHistory} from 'vue-router';
import routes from '@/router/routes/index.js';

const router = createRouter({
    history: createWebHashHistory(),
    routes
});

router.beforeEach((to, form, next) => {
    if(to.name != 'login' && !sessionStorage.getItem("token")){
        next({path: '/'});
    }else{
        next();
    }
});

export default router;