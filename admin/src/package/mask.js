const mask = {
    'backgroundColor': 'rgba(200,200,200,0.8)',
    'img': '/img/loading.png',
    create(){
        const markId = 'mask_'+Date.now();
        const div = document.createElement("div");
        div.id = markId;
        const template = `
            <style>
                #${markId}{
                    background-color: ${this.backgroundColor};
                    position: fixed;
                    top: 0;
                    right: 0;
                    bottom: 0;
                    left: 0;
                    z-index: 0;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                }
                #${markId} img{
                    width: 40px;
                    animation-name: example;
                    animation-iteration-count: infinite;
                    animation-duration: 5s;
                }
                @keyframes example {
                    0% {
                        transform: rotate(0deg);
                    }
                    100% {
                        transform: rotate(360deg);
                    }
                }
            </style>
            <img src="${this.img}">
        `;
        div.innerHTML = template;
        document.body.append(div);
        return markId;
    },
    remove(markId){
        document.body.querySelector("#"+markId).remove();
    }
}
export default mask;