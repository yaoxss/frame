import mask from "@/package/mask.js";

// const requestUrl = 'http://frame.com';
const requestUrl = 'http://39.96.14.48:81';
const http = {
    post(url,data = {}){
        url = requestUrl + url;
        const formData = new FormData();
        // 二级可以采用以上方式 暂时采用json上传
        // formData.append("a[1]",1);
        // formData.append("a[2]",2);
        for(let i in data){
            if(typeof data[i] === 'object'){
                formData.append(i,JSON.stringify(data[i]));
            }else{
                formData.append(i,data[i]);
            }
        }
        return new Promise((resolve) => {
            const maskId = mask.create();
            const request = new XMLHttpRequest();
            request.open("POST",url);
            const token = sessionStorage.getItem('token');
            if(token){
                // https://juejin.cn/post/6844903949233848328
                // request.setRequestHeader("Content-type","multipart/form-data; charset=utf-8; boundary=" + Math.random().toString().substring(2)); 
                // request.setRequestHeader('X-Requested-With','XMLHttpRequest');
                request.setRequestHeader('Authorization',token);
            }
            request.responseType = 'json';
            // 当一个XMLHttpRequest请求完成的时候会触发load 事件。
            request.addEventListener("load",(e) => {
                mask.remove(maskId);
                resolve(e.currentTarget.response);
            });
            request.send(formData);
        });
    },
    test(){
        console.log('test');
    }
}
export default {
    install: app => {
        app.config.globalProperties.post = http.post;
    }
} ;