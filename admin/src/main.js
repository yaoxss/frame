import { createApp } from 'vue'
import App from './App.vue'
import router from './router/index'

import vueRow from "@/components/formStyle/Row.vue"
import vueInput from "@/components/formStyle/Input.vue"
import vueRowInput from "@/components/formStyle/RowInput.vue"
import vueButton from "@/components/formStyle/Button.vue"
import vueTable from "@/components/table/Table.vue"
import vueRowSelect from "@/components/formStyle/RowSelect.vue"
import vueRowTextarea from "@/components/formStyle/RowTextarea.vue"
import http from '@/package/http.js';

const app = createApp(App);
app.use(router).use(http).mount('#app');
app.component('vueRow',vueRow);
app.component('vueInput',vueInput);
app.component('vueRowInput',vueRowInput);
app.component('vueButton',vueButton);
app.component('vueTable',vueTable);
app.component('vueRowSelect',vueRowSelect);
app.component('vueRowTextarea',vueRowTextarea);

