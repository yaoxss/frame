<?php
namespace yaoxs;

// 请求与响应的相关方法
class Http{
    // $validates 验证规则数组
    public function P($validates = []){
        $post = $_POST ?: file_get_contents('php://input');
        if(is_string($post)){
            $post = json_decode($post,true,10);
        }
        if($post == null){
            $post = [];
        }
        // 详细看文档 https://www.php.net/manual/zh/filter.filters.validate.php
        $validatesConstantAbbreviation = [
            'email' => [
                'rule' => FILTER_VALIDATE_EMAIL,
                'error' => '不是电子邮件地址'
            ], 
            'url' => [
                'rule' => FILTER_VALIDATE_URL,
                'error' => '不是有效的url'
            ], 
            'ip' => [
                'rule' => FILTER_VALIDATE_URL,
                'error' => '不是有效的ip'
            ], 
            'int' => [
                // 验证该值是否为整数。可选地来自指定的范围，并在成功时转换为 int。
                'rule' => FILTER_VALIDATE_INT,
                'error' => '不是int类型'
            ], 
            'float' => [
                'rule' => FILTER_VALIDATE_FLOAT,
                'error' => '不是float类型'
            ], 
            'boolean' => [
                'rule' => FILTER_VALIDATE_BOOLEAN,
                'error' => '不是1、true、on和yes'
            ]
        ];
        // $validates = [
        //     'email' => ['email'],
        //     'url' => ['url'],
        //     'ip' => ['ip'],
        //     'int' => ['int',0,200]
        // ];
        foreach($post as $key => &$value){
            // 这里只转义html标签
            $value = htmlentities($value);
            // 判断该值是否需要验证
            if(isset($validates[$key])){
                $rule = $validatesConstantAbbreviation[$validates[$key][0]]['rule'];
                if(isset($validates[$key][1])){
                    $options = [];
                    $options['options'] = [];
                    $options['options']['default'] = false;
                    $options['options']['min_range'] = $validates[$key][1];
                    if(isset($validates[$key][2])){
                        $options['options']['max_range'] = $validates[$key][2];
                    }
                    $result = filter_var($value, $rule, $options);
                }else{
                    $result = filter_var($value, $rule);
                }
                if(!$result){
                    // 返回单条的验证错误信息
                    return [
                        'result' => false, 
                        'msg' => $key.$validatesConstantAbbreviation[$validates[$key][0]]['error']
                    ];
                }
            }
        }
        return $post;
    }

    public function getPost(){
        $post = $_POST ?: file_get_contents('php://input');
        if(is_string($post)){
            $post = json_decode($post,true,10);
        }
        if($post == null){
            $post = [];
        }
        return $post;
    }
}

// // 对于接受选项的过滤器，请使用此格式
// $options = array(
//     'options' => array(
//         'default' => 3, // 过滤器失败时返回的值
//         // 可以继续写过滤器接收其它选项
//         'min_range' => 0
//     ),
//     'flags' => FILTER_FLAG_ALLOW_OCTAL,
// );
// $var = filter_var('0755', FILTER_VALIDATE_INT, $options);
// // 对于仅接受标志的过滤器，您可以直接将其传递
// $var = filter_var('oops', FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
// // 对于仅接受标志的过滤器，您还可以将其作为数组传递
// $var = filter_var('oops', FILTER_VALIDATE_BOOLEAN,
//                   array('flags' => FILTER_NULL_ON_FAILURE));
// // 回调类型过滤器
// function foo($value)
// {
//     // 预期值: Surname, GivenNames
//     if (strpos($value, ", ") === false) return false;
//     list($surname, $givennames) = explode(", ", $value, 2);
//     $empty = (empty($surname) || empty($givennames));
//     $notstrings = (!is_string($surname) || !is_string($givennames));
//     if ($empty || $notstrings) {
//         return false;
//     } else {
//         return $value;
//     }
// }
// $var = filter_var('Doe, Jane Sue', FILTER_CALLBACK, array('options' => 'foo'));