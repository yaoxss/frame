<?php
namespace yaoxs;
use config\Config;
use PDO;

class DB{
    private $dbms = 'mysql';
    private $host;
    private $dbName;
    private $user;
    private $pass;
    private $db;
    
    public function __construct(){
        $mysqlDefaultConfig = Config::$mysqlConfig;
        $this->host = $mysqlDefaultConfig['host'];
        $this->dbName = $mysqlDefaultConfig['database'];
        $this->user = $mysqlDefaultConfig['username'];
        $this->pass = $mysqlDefaultConfig['password'];
        $dns = 'mysql:host='.$this->host.';dbname='.$this->dbName;
        $this->db = new PDO($dns,$this->user,$this->pass);    
    }

    public function select($sql,$bind = []){
        $pdoStatement = $this->db->prepare($sql,[PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
        foreach($bind as $key => $value){
            $pdoStatement->bindParam($key,$value);
        }
        $pdoStatement->execute();
        return $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function insert($table,$insertData){
        $arr = $this->assemblyInsertSql($table,$insertData);
        $pdoStatement = $this->db->prepare($arr['sql'],[PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
        $bind = $arr['bind'];
        foreach($bind as $key => $value){
            $pdoStatement->bindValue($key,$value);
        }
        return $pdoStatement->execute();
    }

    private function assemblyInsertSql($table,$insertData){
        $fieldStr = '';
        $insertBindStr = '';
        $bind = [];
        if(!isset($insertData[0])){
            // 说明单条
            $fieldStr = implode(',',array_keys($insertData));
            $insertBindValue = [];
            foreach($insertData as $key => $val){
                $insertBindValue[] = ':'.$key;
                $bind[':'.$key] = $val;
            }
            $insertBindStr = implode(',',$insertBindValue);
            $sql = "INSERT INTO {$table} ({$fieldStr}) VALUES({$insertBindStr});";
        }else{
            $fieldStr = implode(',',array_keys($insertData[0]));
            $insertbindValueArr = [];
            foreach($insertData as $key => $val){
                $insertBindValue = [];
                foreach($val as $k => $v){
                    $insertBindValue[] = ':'.$k.$key;
                    $bind[':'.$k.$key] = $v;
                }
                $insertbindValueArr[] = '('.implode(',',array_values($insertBindValue)).')';
            }
            $insertBindStr = implode(',',$insertbindValueArr);
            $sql = "INSERT INTO {$table} ({$fieldStr}) VALUES {$insertBindStr};";
        }
        return [
            'sql' => $sql,
            'bind' => $bind
        ];
    }

    // UPDATE table_name SET field1=new-value1, field2=new-value2
    public function update($table,$update = [],$where = '',$whereBind = []){
        if(!$where || !$update){
            // 不允许没有条件的更新
            return false;
        }
        $sql = "UPDATE {$table} SET ";
        $bind = [];
        $updateStr = [];
        foreach($update as $key => $value){
            $updateStr[] = $key.'=:'.$key;
            $bind[':'.$key] = $value;
        }
        $updateStr = implode(',',$updateStr);
        
        foreach($whereBind as $key => $value){
            $bind[$key] = $value;
        }
        $sql .= $updateStr. ' where ' .$where;
        $pdoStatement = $this->db->prepare($sql,[PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
        foreach($bind as $key => $value){
            $pdoStatement->bindValue($key,$value);
        }
        $pdoStatement->execute();
        return $pdoStatement->rowCount();
    }

    public function delete($sql,$bind = []){
        $pdoStatement = $this->db->prepare($sql,[PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
        foreach($bind as $key => $value){
            $pdoStatement->bindParam($key,$value);
        }
        $pdoStatement->execute();
        return $pdoStatement->rowCount();
    }
}
?>