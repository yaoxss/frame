<?php
namespace yaoxs;

class Controller{
    // 獲取當前域名包含http或https
    protected function getServerName($port = false){
        // $_SERVER['HTTPS'] 如果脚本是通过 HTTPS 协议被访问，则被设为一个非空的值。
        //  || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')
        $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on')) ? 'https://' : 'http://';
        $serverName = '';
        if($port){
            // HTTP服务的默认端口就是80，HTTP_HOST都没有显示这个端口号。ssl链接443，那么端口号将被显示出来。
            $serverName = $http_type.$_SERVER['HTTP_HOST'];
        }else{
            $serverName = $http_type.$_SERVER['SERVER_NAME'];
        }
        return $serverName;
    }

    protected function getIP()
    {
        $ip = '';
        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        }
        if (isset($_SERVER["HTTP_CLIENT_IP"])) {
            $ip = $_SERVER["HTTP_CLIENT_IP"];
        }
        $ip = $_SERVER["REMOTE_ADDR"];
        return $ip;
    }
    
    protected function curl_get_https($url){
        $curl = curl_init(); // 启动一个CURL会话
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // 跳过证书检查
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);  // 从证书中检查SSL加密算法是否存在
        $tmpInfo = curl_exec($curl);     //返回api的json对象
        //关闭URL请求
        curl_close($curl);
        return $tmpInfo;    //返回json对象
    }

    protected function echoJson($data){
        header('Content-Type: application/json; charset=UTF-8');
        echo json_encode($data,JSON_UNESCAPED_UNICODE);
        die;
    }

    protected function echoStatusJson($array){
        if($array === false){
            $this->echoJson(['status' => 'error']);
        }
        $this->echoJson([
            'status' => 'success',
            'info' => $array
        ]);
    }
}
