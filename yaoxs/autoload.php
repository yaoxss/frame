<?php
spl_autoload_register('autoload',true,true);

function autoload($className){
    // if(strpos($className,'yaoxs') === 0){
        
    // }
    // 直接根据命名空间加载，相关文件
    $className = str_replace("\\",DS,$className);
    require_once(ROOT.DS.$className.'.php');
}

// 获取P(平台)C(控制器)A(方法名)
function getPCA(){
    $uriConfig = config\Config::$routeConfig;
    $uri = $_SERVER['REQUEST_URI'];
    if(strpos($_SERVER['REQUEST_URI'],'/index.php') === 0){
        $uri = explode('/index.php',$_SERVER['REQUEST_URI'])[1];
        $uri = $uri ?: '/';
    }
    return $uriConfig[$uri] ?? false;
}
list($P,$C,$A) = getPCA();

$className = 'http\\'.$P.'\\'.ucwords($C);
// echo $className;die;
$obj = new $className();
$obj->$A();
