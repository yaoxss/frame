<?php
namespace config;

class Config{
    public static $mysqlConfig = [
        'host'=>'localhost',
        'database'=>'blog',
        'username'=>'root',
        'password'=>'123456',
        'prefix'=>''
    ];
    
    // ['平台','控制器','方法']
    public static $routeConfig = [
        '/' => ['api','index','index'],
        '/api/home/index' => ['api','index','index'],
        '/api/home/getAllArticleCategory' => ['api','index','getAllArticleCategory'],
        '/api/home/getArticleList' => ['api','index','getArticleList'],
        '/api/home/getarticlerow' => ['api','index','getArticleRow'],
        '/api/auth/login' => ['api','Login','login'],
        '/api/adminArticle/index' => ['api','AdminArticle','index'],
        '/api/adminArticle/create' => ['api','AdminArticle','create'],
        '/api/adminArticle/getAllArticleCategory' => ['api','AdminArticle','getAllArticleCategory'],
        '/api/adminArticle/getArticleRow' => ['api','AdminArticle','getArticleRow'],
        '/api/adminArticle/update' => ['api','AdminArticle','update'],
        '/api/adminArticle/createTemplate' => ['api','AdminArticle','createTemplate'],
        '/api/adminArticleCategory/index' => ['api','AdminArticleCategory','index'],
        '/api/adminArticleCategory/create' => ['api','AdminArticleCategory','create'],
        '/api/adminArticleCategory/row' => ['api','AdminArticleCategory','row'],
        '/api/adminArticleCategory/update' => ['api','AdminArticleCategory','update'],
    ];
    
}

?>