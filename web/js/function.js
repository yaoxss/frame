// const requestUrl = 'http://frame.com';
const requestUrl = 'http://39.96.14.48:81';

function post(url,data = {}){
    url = requestUrl + url;
    const formData = new FormData();
    // 二级可以采用以上方式 暂时采用json上传
    // formData.append("a[1]",1);
    // formData.append("a[2]",2);
    for(let i in data){
        if(typeof data[i] === 'object'){
            formData.append(i,JSON.stringify(data[i]));
        }else{
            formData.append(i,data[i]);
        }
    }
    return new Promise((resolve) => {
        const request = new XMLHttpRequest();
        request.open("POST",url);
        request.responseType = 'json';
        // 当一个XMLHttpRequest请求完成的时候会触发load 事件。
        request.addEventListener("load",(e) => {
            resolve(e.currentTarget);
        });
        request.send(formData);
    });
}

// 建议showPage 为单数 双数应该是 showPage + 1
function getPageDate(page,size,count,showPage){
    const returnData = {
        'count': count,
        'page': page,
        'size': size,
        'pageArray': [],
        'previousPage': false,
        'nextPage': false
    };
    if(count <= 0) return returnData;
    if(size >= count){
        returnData.pageArray.push(1);
        return returnData;
    }
    const centre = Math.ceil(showPage/2);
    // page 左边需要显示的个数
    let beforePageNum = 0;
    // page 右边需要显示的个数
    let afterPageNum = 0;
    const subtraction = page - centre;
    if(subtraction < 0){
        afterPageNum = Math.abs(subtraction);
        // centre - 1正常情况下左侧需要保持的个数
        beforePageNum =  centre - 1 - Math.abs(subtraction); 
    }else{
        // 单数和双数的情况
        showPage%2 == 1 ? beforePageNum = centre-1 : beforePageNum = centre; 
    }
    // 右边能够显示的长度
    const afterShowLength = Math.ceil(count/size) -page;
    showPage%2 == 1 ? afterPageNum += centre-1 : afterPageNum += centre;
    if(afterShowLength < afterPageNum){
        // 将右侧舍弃掉的页码补充到左侧
        let i = page - beforePageNum;
        let supplementNum = afterPageNum - afterShowLength;
        while(i > 1 && supplementNum > 0){
            ++beforePageNum;
            --i;
            --supplementNum;
        }
        // 显示右侧的余下页码
        afterPageNum = afterShowLength;
    }
    const leftRange = page - beforePageNum;
    const rightRange = page + afterPageNum;
    for(let i = leftRange ; i <= rightRange ; i++){
        returnData.pageArray.push(i);
    }
    if(page > 1){
        returnData.previousPage = page - 1;
    }
    if(page < rightRange){
        returnData.nextPage = page + 1;
    }
    return returnData;
}