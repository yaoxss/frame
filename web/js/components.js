const navComponents = {
    data() {
        return {
            articleCategory: [],
            // maskShow: false,
        }
    },
    props: ['modelValue','lable'],
    emits: ['showCategoryArticle','update:modelValue'],
    template: `
        <nav v-show="modelValue" style="z-index:1000;">
            <avatar>
                <img v-if="isTemplate == 2" src="./../img/my.jpg">
                <img v-else src="./img/my.jpg">
                <p>yaoxs</p>
            </avatar>
            <div>
                <li @click="gotoUrl('./index.html')">
                    <span class="iconfont">&#xe626;</span>
                    <span>首页</span>
                </li>
                <li v-if="lable != 'search'" @click="listShow($event)">
                    <span class="iconfont">&#xe647;</span>
                    <span>文章分类</span>
                    <div class="show">
                        <img src="img/icon/a.png" >
                    </div>
                </li>
                <ul class="children">
                    <li v-for="(item,index) in articleCategory" :key="index" @click="showCategoryArticle(item.id)" >{{item.title}}</li>
                </ul>
                <li @click="gotoUrl('./search.html')">
                    <span class="iconfont">&#xe60e;</span>
                    <span>检索</span>
                </li>
            </div>
        </nav>`,
    created(){
        console.log(this.isTemplate)
        let mask = this.getMask(999);
        document.body.appendChild(mask);
        this.$watch('modelValue', (newVal) => {
            if(document.querySelector('html').clientWidth >= 800){   
                return;
            }
            if(newVal == true){
                mask.style.display = "block";
            }
        })
        if(document.querySelector('html').clientWidth < 800){   
            this.$emit('update:modelValue', false)
        }else{
            this.$emit('update:modelValue', true)
        }
        window.addEventListener('resize',() => {
            if(document.querySelector('html').clientWidth < 800){   
                this.$emit('update:modelValue', false)
            }else{
                this.$emit('update:modelValue', true)
            }
        });
        post('/api/home/getAllArticleCategory').then((e) => {
            if(e.status == 200)
                this.articleCategory = e.response.articleCategory;
        });
    },
    methods: {
        listShow(event){
            const currentDom = event.currentTarget;
            const children = currentDom.nextElementSibling;
            if(children.tagName != 'UL'){
                return ;
            }
            if(children.style.display == '' || children.style.display == 'none'){
                children.style.display = 'block';
                currentDom.querySelector('.show').style.transform = "rotate(180deg)";
            }else{
                currentDom.querySelector('.show').style.transform = "rotate(0deg)";
                children.style.display = 'none'
            }
        },
        showCategoryArticle(id){
            this.$emit('showCategoryArticle',id);
        },
        gotoUrl(url){
            location.href = url;
        },
        getMask(zIndex){
            let mask = document.createElement('div');
            mask.style.margin = 0;
            mask.style.padding = 0;
            mask.style.position = 'fixed';
            mask.style.top = 0;
            mask.style.right = 0;
            mask.style.bottom = 0;
            mask.style.left = 0;
            mask.style.zIndex = zIndex;
            mask.style.display = "none";
            mask.setAttribute('class','mask-bg');
            mask.addEventListener('click',(e) => {
                e.target.style.display = 'none';
                this.$emit('update:modelValue', false);
            });
            return mask;
        }
    }
};