<?php
$rad = M_PI;
// 求角度的方法
$deg = rad2deg(acos(4/8));
// echo $deg;die;
function index(){
    // 獲取圖片路徑
    $src = "banner.png";
    // 獲取圖片信息
    // $info = getimagesize($src);

    // // 通過編號獲取圖像類型
    // $type = image_type_to_extension($info[2],false);
    // // dump($type);

    // // 構建對應拷貝圖片到内容的函數
    // $fun = "imagecreatefrom".$type;

    // 拷貝圖片到内存
    $image = imagecreatefrompng($src);
    $pngw = imagesx($image);
    $pngh = imagesy($image);

    // 準備操作對象加文字水印
    // 服務器對於字體
    $font = "D:/project/test/imgcode.ttf";

    // 水印文字
    $watermark = "Copy!Copy!Copy!CopyCopy!Copy!Copy!Copy!CopyCopy!Copy!!Copy!Copy!y!CopyCopy!Copy!Copy!Copy";
    $angle = rad2deg(acos(round($pngw/sqrt($pngh*$pngh + $pngw*$pngw),8)));
    $arr = imagettfbbox(10,0-$angle,$font,$watermark);
    // var_dump($arr);die;
    $w = abs($arr[0] - $arr[2]);
    $h = abs($arr[1] - $arr[3]);
    // 設置字體顔色和透明度（圖片資源，紅，綠，藍，透明度）
    $color = imagecolorexactalpha($image, 0, 0, 255, 0);

    
    // echo $angle;
    // 寫入文字（圖片資源，字體大小，旋轉角度，坐標X,坐標Y,顔色，字體文件，内容）
    // echo $h.'----'.$w;die; 
    $textw = ($pngw-$w)/2;
    $texth = ($pngh-$h)/2;
    imagettftext($image, 10, 0-$angle, $textw, $texth, $color, $font, $watermark);

    // 輸出圖片

    // 瀏覽器輸出
    // header("Content-type:".$info['mime']);
    // $funOut = "image".$type;

    // imagepng($image);

    // 保存圖片
    imagepng($image,'bg_res.png');

    // 銷毀圖片資源
    imagedestroy($image);
}
index();
?>